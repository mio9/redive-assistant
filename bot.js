const { AkairoClient, CommandHandler, ListenerHandler } = require("discord-akairo");
const NeDB = require("nedb-promises");
const schedule = require('node-schedule');
const storageLib = require("./api/storage-lib");

class BotClient extends AkairoClient {
   constructor() {
      super({
         ownerID: ["212825259697635328", "215752403872382978"],
      });

      // database
      console.log("Initializing database...");
      this.db = require('./classes/Database');

      // cache database
      this.cachedb = NeDB.create();

      // Settings manager
      this.settings = storageLib.SettingsManager;
      this.serverInfo = storageLib.InfoManager;
      this.bosses = storageLib.BossManager;

      // command handler
      this.commandHandler = new CommandHandler(this, {
         directory: "./commands/",
         defaultCooldown: 1000,
         ignoreCooldown: ["674265066824138753", "212825259697635328"],
         ignorePermissions: ["674265066824138753","212825259697635328"],
         allowMention: true,
         prefix: async (message) => {
            if (message.guild) {
               // query cache
               let doc = await this.cachedb.findOne({ _id: message.guild.id });
               if (!doc) {
                  // no cache
                  // query main db
                  let dbSettings = await this.settings.getAll(message.guild.id)
                  if (!dbSettings || !dbSettings.prefix) {
                     // write to cache
                     this.cachedb.insert({ _id: message.guild.id, prefix: "." });
                     return ".";
                  } else {
                     const { prefix } = dbSettings
                     this.cachedb.insert({
                        _id: message.guild.id,
                        prefix
                     });

                     return dbSettings.prefix;
                  }
               } else {
                  // read from cache
                  return doc.prefix;
               }
            }
         },
      });
      this.commandHandler.loadAll();

      // the listener handler
      this.listenerHandler = new ListenerHandler(this, {
         directory: './listeners/'
      });
      this.listenerHandler.setEmitters({
         commandHandler: this.commandHandler,
         listenerHandler: this.listenerHandler
      });
      this.commandHandler.useListenerHandler(this.listenerHandler);
      this.listenerHandler.loadAll();

   }
}

console.log(
   "[!] Bot running in: production mode, with default prefix: . "
);
const client = new BotClient();
client.login(require("./private_keys/token.json").token).then(() => {
   console.log("Re:Dive Assistant logged in as " + client.user.username);
});


// daily reset
let rule = new schedule.RecurrenceRule();

rule.tz = 'Asia/Hong_Kong';
rule.second = 0;
rule.minute = 0;
rule.hour = 5;

let resetJob = schedule.scheduleJob(rule, function () {
   client.emit("dailyReset")
});

client.resetTimer = resetJob