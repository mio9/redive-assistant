const strings = require("../lib/strings.json");
const ResultObject = require("./ResultObject");
const parse = require("../api/parse-progress");
const InfoManager = require("./InfoManager");


/**
 * Manage knife reports in database
 */
class KnifeReportManager {
   constructor(db) {
      this.db = db
      this.infoManager = new InfoManager(db)
   }
   //-------------------------------------------------------------------
   /**
    * Submit a new report to db
    * @param {Snowflake} guildID Guild ID
    * @param {ReportObject} report Report data inside an object
    * @param {boolean} isAdministrator Administrative bypass 
    * @returns {ResultObject} Status of the submitted report
    */
   async add(guildID, report, isAdministrator = false) {

      // Add a report using informations inside the object.
      let serverDocRef = this.db
         .collection("servers")
         .doc(guildID)

      let serverDoc = await serverDocRef.get()

      // has the server setup the board?
      if (!serverDoc.data().settings.board_channel ||
         serverDoc.data().settings.board_channel == "") {
         return new ResultObject(false, strings.board.missing)
      };


      const dbProgressNumber = serverDoc.data().current_progress_num;
      const dbProgressStart = parse.getWeekArray(dbProgressNumber)[0];

      // bypass range checking if administrator

      if (isAdministrator == false) {

         if (report.progress_num < dbProgressNumber) {
            // cannot report before
            return new ResultObject(false, strings.knifeReport.below_range)
         }
         else if (report.progress_num > dbProgressStart + 4 + serverDoc.data().settings.user_max_future_knife_weeks * 5) {
            // cannot report beyond
            return new ResultObject(false, strings.knifeReport.above_range)
         };

      };



      // read main db check max boss queue of that week boss
      // const dbProgressNumber = serverDoc.data().current_progress_num;
      const dbq2 = serverDocRef.collection("reports").where('progress_num', '==', report.progress_num).get()
      const dbq3 = serverDocRef.collection("reports").where('user_id', '==', report.user_id).where('is_compensate', '==', false).get()
      const dbq4 = serverDocRef.collection("reports").where('user_id', '==', report.user_id).where('is_compensate', '==', true).get()

      const [dbReportDataSnapshot, dbUserReportsDataSnapshot, dbUserCompensateReportsDataSnapshot] = await Promise.all([dbq2, dbq3, dbq4]);

      // set max knife count to server default
      const bossMaxKnife = serverDoc.data().settings.default_max_knife;
      const userMaxKnife = serverDoc.data().settings.user_max_knife;
      const userMaxCompensateKnife = serverDoc.data().settings.user_max_compensate_knife;

      // check current boss full
      if (dbReportDataSnapshot.size >= bossMaxKnife) {
         return new ResultObject(false, strings.knifeReport.boss_limit);
      }
      // check user pass normal knife limit
      else if ((dbUserReportsDataSnapshot.size >= userMaxKnife) && (report.is_compensate == false)) {
         return new ResultObject(false, strings.knifeReport.knife_limit);
      }
      //check user pass compensate knife limit
      else if ((dbUserCompensateReportsDataSnapshot.size >= userMaxCompensateKnife) && (report.is_compensate == true)) {
         return new ResultObject(false, strings.knifeReport.compensate_limit)
      };


      // everything is fine, reporting knife


      let pushingObject = {

         user_id: report.user_id,
         username: report.username,
         time: report.time,

         progress_num: report.progress_num,
         is_completed: report.is_completed,

         type: report.type,
         is_compensate: report.is_compensate,
         comment: report.comment,

      };

      serverDocRef.collection("reports").doc().set(pushingObject);

      let result = new ResultObject(true, pushingObject);
      return result;
   };


   //-------------------------------------------------------------------
   /**
    * Delete an existing report.
    * @param {import("discord.js").Snowflake} guildID Guild ID
    * @param {array} reportArr Array of reports
    * @returns {void} 
    */
   async delete(guildID, reportArr) {

      const query = this.db
         .collection("servers")
         .doc(guildID)
         .collection("reports");

      // for (let i in reportArr) {
      //    query.doc(reportArr.id).delete()
      // };
      reportArr.forEach(report => {
         query.doc(report.id).delete();
      })
      // Delete corresponding report.
      // Returns the details on deleted knife report(s).
      return;

   };

   //-------------------------------------------------------------------
   /**
    * Get an array of report IDs that matched the parameters.
    * @param {import("discord.js").Snowflake} guildID Guild ID
    * @param {object} reportQuery Report data to be matched
    * @param {string} timeSortOrder `desc (default)` or `asc`
    * @returns {array} Array of report IDs that matched
    */
   async getReportID(guildID, reportQuery, timeSortOrder = "desc") {

      const responseReports = await this.getReports(guildID, reportQuery, timeSortOrder)

      let arr = [];
      responseReports.forEach(e => {
         arr.push(e.id);
      });
      return arr;
   };

   //-------------------------------------------------------------------
   /**
    * Get an array of reports, given an array of report IDs.
    * @param {import("discord.js").Snowflake} guildID Guild ID
    * @param {array} reportID Unique report ID
    * @returns {array} Array of report data
    */
   async getReportData(guildID, reportID) {

      const query = this.db
         .collection("servers")
         .doc(guildID)
         .collection("reports")
         .doc(reportID);

      const snapshot = await query.get();
      const result = snapshot.data();

      return result;
   };


   /**
    * Get an array of reports with id and data [{id: docs_id, data: docs_data}, ...]
    * @param {import("discord.js").Snowflake} guildID Guild ID
    * @param {object} reportQuery Report data to be matched
    * @param {string} timeSortOrder `desc` (default) or `asc`
    */
   async getReports(guildID, reportQuery, timeSortOrder = "desc") {
      let ref = this.db
         .collection("servers")
         .doc(guildID)
         .collection("reports")

      /*
      progress_num
      user_id
      is_complete
      */

      if (!isNaN(reportQuery.progress_num)) {
         ref = ref.where('progress_num', '==', reportQuery.progress_num);
      };

      if (reportQuery.user_id) {
         ref = ref.where('user_id', '==', reportQuery.user_id);
      };

      if (reportQuery.is_completed != null) {
         ref = ref.where('is_completed', '==', reportQuery.is_completed);
      };

      if (reportQuery.is_compensate != null) {
         ref = ref.where('is_compensate', '==', reportQuery.is_compensate);
      };



      const snapshot = await ref.get()

      if (snapshot.empty) {
         return [];
      };

      let sorter = [];

      snapshot.forEach(doc => {
         sorter.push({ id: doc.id, data: doc.data() })
      })

      //sort in different direction
      if (timeSortOrder == "asc" || timeSortOrder == "ascending") {
         sorter.sort((a, b) => a.data.time - b.data.time);
      } else {
         sorter.sort((a, b) => b.data.time - a.data.time);
      };
      return sorter;
   }

   //-------------------------------------------------------------------
   /**
    * Edit data of an existing report
    * @param {import("discord.js").Snowflake} guildID Guild ID
    * @param {string} reportID the unique ID of the report being modified
    * @param {object} reportEditObject Fields to replace
    * @returns {object} Edited report data
    */


   async edit(guildID, reportID, reportEditObject) {

      await this.db
         .collection("servers")
         .doc(guildID)
         .collection("reports")
         .doc(reportID)
         .update(reportEditObject)

      const result = await this.getReportData(guildID, reportID)
      return result;
   };

   /**
    * Delete all existing reports
    * @param {import("discord.js").Snowflake} guildID Guild ID 
    * @param {*} db Database to work on, Default as `this.db`
    * @param {number} batchSize Amount of records to be deleted in each operation
    */
   async deleteAllReport(guildID, db = this.db, batchSize = 10) {

      const collectionRef = db
         .collection("servers")
         .doc(guildID)
         .collection("reports");
      const query = collectionRef.limit(batchSize);

      // Delete all existing knife reports
      return new Promise((resolve, reject) => {
         this._deleteQueryBatch(db, query, resolve).catch(reject);
      });
   };

   /**
   * Delete all existing reports for a specific user
   * @param {import("discord.js").Snowflake} guildID Guild ID 
   * @param {*} userID 
   * @param {*} db Database to work on, Default as `this.db`
   * @param {number} batchSize Amount of records to be deleted in each operation
   */
   async deleteAllUserReport(guildID, userID, db = this.db, batchSize = 10) {
      const collectionRef = db
         .collection("servers")
         .doc(guildID)
         .collection("reports")
         .where("user_id", "==", userID);

      const query = collectionRef.limit(batchSize);

      // Delete all existing knife reports
      return new Promise((resolve, reject) => {
         this._deleteQueryBatch(db, query, resolve).catch(reject);
      });
   };

   /**
    * Ported from firebase documentation. Internal function.
    * @param {*} db Database to work on
    * @param {*} query Query to delete
    * @param {*} resolve 
    */
   async _deleteQueryBatch(db, query, resolve) {
      const snapshot = await query.get();

      const batchSize = snapshot.size;
      if (batchSize == 0) {
         // When there are no documents left, we are done
         resolve();
         return;
      }

      // Delete documents in a batch
      const batch = db.batch();
      snapshot.docs.forEach((doc) => {
         batch.delete(doc.ref);
      });
      await batch.commit();

      // Recursion on the next process tick, to avoid exploding the stack.
      process.nextTick(() => {
         this._deleteQueryBatch(db, query, resolve);
      });
   }

};

module.exports = KnifeReportManager;
