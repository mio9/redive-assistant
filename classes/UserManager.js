const strings = require("../lib/strings.json");

class UserManager {

   constructor(db) {
      this.db = db
   }

   /*
   user file format:

   user_id: string
   holding_compensate: boolean 
   */

   /**
    * Edit the compensate status of a specific user
    * @param {*} guildID Guild ID
    * @param {*} userID User ID
    * @param {*} type `next` or `finish` to auto-set, or `true` or `false` to force overwrite
    */
   async compensateEdit(guildID, userID, type) {

      // switch compensate edit type

      let newCompensate;

      const serverQueryRef = this.db
         .collection("servers")
         .doc(guildID)
         .collection("users")
         .doc(userID);

      const holdingCompensate = await this.compensateCheck(guildID, userID)

      switch (type) {

         case "next":
            newCompensate = !holdingCompensate;
            break;

         case "finish":
         case "false":
            newCompensate = false;
            break;

         case "true":
            newCompensate = true
            break;

         default:
            return null;
      };

      // update

      const updateUserData = {
         holding_compensate: newCompensate,
         previous_compensate: holdingCompensate
      };
      await serverQueryRef.update(updateUserData);

      return newCompensate;
   };

   async compensateRevert(guildID, userID) {
      const serverQueryRef = this.db
         .collection("servers")
         .doc(guildID)
         .collection("users")
         .doc(userID);

      const userDoc = await serverQueryRef.get()
      const userData = userDoc.data()
      const prevComp = userData.previous_compensate;

      if (prevComp != null) {
         const updateUserData = {
            holding_compensate: prevComp,
            previous_compensate: null
         };
         await serverQueryRef.update(updateUserData);
         return prevComp;
      } else {
         return strings.undo.exceed_limit;
      }

   };


   async compensateCheck(guildID, userID) {

      const serverQueryRef = this.db
         .collection("servers")
         .doc(guildID)
         .collection("users")
         .doc(userID);

      const userDoc = await serverQueryRef.get();

      if (!userDoc.exists) {
         const pushUserData = {
            holding_compensate: false,
            previous_compensate: null
         };
         await serverQueryRef.set(pushUserData);
         return false;
      }

      const userData = userDoc.data();

      return userData.holding_compensate;


   };

   async resetAllCompensate(guildID, db = this.db, batchSize = 10) {

      const collectionRef = db
         .collection("servers")
         .doc(guildID)
         .collection("users")
         .where("holding_compensate", "==", true);

      const query = collectionRef.limit(batchSize);

      // overwrite all user compensate status to false
      return new Promise((resolve, reject) => {
         this._overwriteCompStatusBatch(db, query, resolve).catch(reject);
      });
   };

   async _overwriteCompStatusBatch(db, query, resolve) {
      const snapshot = await query.get();

      const batchSize = snapshot.size;
      if (batchSize == 0) {
         // when there are no documents left, we are done
         resolve();
         return;
      }

      // overwrite documents in a batch
      const batch = db.batch();
      snapshot.docs.forEach((doc) => {
         batch.update(doc.ref, { holding_compensate: false, previous_compensate: null });
      });
      await batch.commit();

      // Recursion on the next process tick, to avoid exploding the stack.
      process.nextTick(() => {
         this._overwriteCompStatusBatch(db, query, resolve);
      });
   };

   async resetAll(guildID, db = this.db) {

   }
}

module.exports = UserManager;