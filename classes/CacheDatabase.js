const NeDB = require("nedb-promises");

class CacheDatabase {
  constructor() {
    this.db = NeDB.create();
  }
}

module.exports = new CacheDatabase().db
