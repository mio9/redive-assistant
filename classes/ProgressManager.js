const InfoManager = require('./InfoManager')
const UtilLib = require("../api/util-lib")
const KnifeReportManager = require('./KnifeReportManager')
const strings = require("../lib/strings.json")
const parse = require("../api/parse-progress")

/**
 * Manage current progress through database
 */
class ProgressManager {

   constructor(db) {
      this.db = db
      this.infoManager = new InfoManager(db)
      this.knifeReportManager = new KnifeReportManager(db)
   };

   /**
    * Move target boss to the next available target
    * @param {string} guildID Guild ID
    * @param {object} message Message
    */
   async nextBoss(guildID, message) {

      // get current progress, and unparse to current week boss and stage
      const currentProgressNum = await this.infoManager.get(guildID, "current_progress_num");
      const nextProgressNum = currentProgressNum + 1;
      const currentWeek = parse.unparse(currentProgressNum, "week");
      const currentBoss = parse.unparse(currentProgressNum, "boss");

      // get players on next boss
      const reportQuery = {
         progress_num: nextProgressNum
      };

      // pulling knife report data
      const reportArr = await this.knifeReportManager.getReports(guildID, reportQuery, "asc");
      let pingMsg = "";
      let pingUserStr = "";

      if (reportArr.length > 0) {
         // if this boss have report
         pingMsg += strings.next.ping_message_start + "\n";
         let userID;
         let pingIDArr = []; //saves which ID have already been added to the message

         for (let i in reportArr) {
            userID = reportArr[i].data.user_id;
            if (!pingIDArr.includes(userID)) {
               pingUserStr += `<@${reportArr[i].data.user_id}> `;  // put ping user into seperate string
               pingIDArr.push(userID);
            };
         };
         pingMsg += pingUserStr; // adding the string at the back

         pingMsg += `\n${strings.next.ping_message_end}`;

      } else if (reportArr.length == 0 &&
         currentWeek != 3 && currentWeek != 10 && currentWeek != 34) { // not changing stage
         // no report in current week, get reports from next week and tag those people
         const nextWeekReportQuery = {
            progress_num: nextProgressNum + 5
         };
         // query next week 
         const nextWeekReportArr = await this.knifeReportManager.getReports(guildID, nextWeekReportQuery, "asc");

         if (nextWeekReportArr.length > 0) {
            pingMsg += strings.next.next_week_ping_message_start + "\n";
            let userID;
            let pingIDArr = []; //saves which ID have already been added to the message

            for (let i in nextWeekReportArr) {
               userID = nextWeekReportArr[i].data.user_id;
               if (!pingIDArr.includes(userID)) {
                  pingUserStr += `<@${nextWeekReportArr[i].data.user_id}> `; // put ping user into seperate string
                  pingIDArr.push(userID);
               };
            };
            pingMsg += pingUserStr; // adding the string at the back

            
            const pingMsgEnd = strings.next.next_week_ping_message_end.replace("%week", currentWeek).replace(/(%boss)/g, currentBoss).replace("%nextWeek", currentWeek + 1);

            pingMsg += `\n${pingMsgEnd}`;
         }

         // list of user on current boss (excluding next week case)
         // return pingUserStr;
      }
      if (pingMsg != "") message.channel.send(pingMsg);

      await this.infoManager.update(guildID, "current_progress_num", nextProgressNum)

      // get boss related data from database
      const bossData = await this.infoManager.get(guildID, "boss_data");
      const nextBossFullHP = bossData.boss_max_hp[parse.unparse(nextProgressNum, "stage")][parse.unparse(nextProgressNum, "boss") - 1];
      // update current HP to next boss HP
      await this.infoManager.update(guildID, "current_boss_HP", nextBossFullHP);

      //update return object after altering value
      const returnObj = {
         progressNum: nextProgressNum,
         pingUserStr: pingUserStr
      };

      return returnObj;
   };

   /**
    * Move target boss to the previous target
    * @param {string} guildID Guild ID
    */
   async prevBoss(guildID) {

      const currentProgressNum = await this.infoManager.get(guildID, "current_progress_num");
      const prevProgressNum = currentProgressNum - 1;

      await this.infoManager.update(guildID, "current_progress_num", prevProgressNum);

      // get boss related data from database
      const bossData = await this.infoManager.get(guildID, "boss_data");
      const prevBossFullHP = bossData.boss_max_hp[parse.unparse(prevProgressNum, "stage")][parse.unparse(prevProgressNum, "boss") - 1]

      await this.infoManager.update(guildID, "current_boss_HP", prevBossFullHP);

      //update return object after altering value
      const returnObj = {
         progressNum: prevProgressNum,
      };

      return returnObj;
   };

   /**
    * Get current week AND boss progress in one shot
    * @param {string} guildID 
    */
   // D O N E
   async getWeekBossProgress(guildID) {
      const progressNum = await this.infoManager.get(guildID, "current_progress_num");
      const result = parse.unparseAll(progressNum)
      return { week: result.week, boss: result.boss };
   };

   
   /**
    * Get current progress number
    * @param {string} guildID 
    */
   async getProgress(guildID) {
      return await this.infoManager.get(guildID, "current_progress_num");
   }

   // /**
   //  * @todo Await implementation
   //  * Edit the availablilty of the speficied boss.
   //  * @param {import("discord.js").Snowflake} guildID Guild ID
   //  * @param {number} week Current Week (Starts from 1) 
   //  * @param {number} boss Current Boss (1-5)
   //  * @param {boolean} [isAvailable=false] Status 
   //  */
   // async setBossStatus(guildID, week, boss, isAvailable = false) {
   //    // Update specified boss status to desired isAvailable state.
   //    // Create new record if there isn't an existing one.
   //    return;
   // };

   // /**
   //  * @todo Await implementation
   //  * Manually set the progress of the guild battle.
   //  * @param {import("discord.js").Snowflake} guildID Guild ID 
   //  * @param {number} week Current Week (Starts from 1) 
   //  * @param {number} boss Current Boss (1-5)
   //  * @param {boolean} [deleteRecord=false] Deletion of unused record
   //  */
   // async setProgress(guildID, week, boss, deleteRecord = false) {
   //    // Set the current progress to specified boss.
   //    // If deleteRecord is set to true, when the progress is moving back, 
   //    // it will delete existing reports until the specified boss (including itself).
   //    return;
   // }

   //internal function
   // async _getRef(guildID, week, boss) {
   //    const snapshot = await this.db
   //       .collection("servers")
   //       .doc(guildID)
   //       .collection("bosses")
   //       .where("week", "==", week)
   //       .where("boss", "==", boss).get()

   //    if (snapshot.empty) {
   //       return;
   //    }
   //    // console.log(snapshot)
   //    const ref = this.db.collection("servers")
   //       .doc(guildID)
   //       .collection("bosses").doc(snapshot.docs[0].id)
   //    return ref
   // }

   /**
    * Seriously I don't know how does this function work, but I guess as long as it works, it is fine.
    * Delete all boss records
    * @param {import("discord.js").Snowflake} guildID Guild ID
    */
   // async deleteAllRecord(guildID) {

   //    let db = this.db
   //    let query = db
   //       .collection("servers")
   //       .doc(guildID)
   //       .collection("bosses")
   //    const batch = this.db.batch()

   //    query.stream().on('data', (documentSnapshot) => {
   //       console.log(`Found boss document with name '${documentSnapshot.id}'`);

   //       batch.delete(db
   //          .collection("servers")
   //          .doc(guildID)
   //          .collection("bosses").doc(documentSnapshot.id))
   //    }).on('end', async () => {
   //       await batch.commit()
   //       console.log(`[E][Progress Reset] ${guildID}`);
   //    });
   // }

   /**
    * Get the maximum HP of the current boss at current stage
    * @param {import("discord.js").Snowflake} guildID Guild ID
    */
   async getCurrentMaxHP(guildID) {
      const serverInfo = await this.infoManager.get(guildID)
      const bossMaxHPObj = serverInfo.boss_data.boss_max_hp;
      const wbObj = await this.getWeekBossProgress(guildID);
      const week = wbObj.week; const boss = wbObj.boss;
      // sorry I am too lazy to use switch
      const stage = "11122222223333333333333333333333334".slice(Math.min(week - 1, 34), Math.min(week, 35));
      return bossMaxHPObj[stage][boss - 1];
   };

   /**
    * Reset the progress of the given server
    * @param  {import("discord.js").Snowflake} guildID Guild ID
    */
   async resetProgress(guildID) {

      await this.infoManager.update(guildID, "current_progress_num", 0);
      // get boss related data from database
      const bossData = await this.infoManager.get(guildID, "boss_data");
      const nextBossFullHP = bossData.boss_max_hp[1][0];
      // update current HP to next boss HP
      await this.infoManager.update(guildID, "current_boss_HP", nextBossFullHP);

      return true;
   }

}

module.exports = ProgressManager