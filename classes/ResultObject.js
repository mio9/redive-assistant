/**
 * Represents an object returned by storage operations
 */
class ResultObject {
   constructor(status, data = null) {
      this.status = status
      this.data = data
   }
}

module.exports = ResultObject