const admin = require("firebase-admin");
const serviceAccount = require("../private_keys/redive-assistent-e172339968e7.json");

class Database {
  constructor() {
    this.db = admin.initializeApp({
      credential: admin.credential.cert(serviceAccount),
    })
      .firestore();
  }
}

module.exports = new Database().db
