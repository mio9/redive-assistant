const ReportContentHandler = require("../api/ReportContentHandler");
const UtilLib = require("../api/util-lib");
const parse = require("../api/parse-progress");

class ReportObject {

   /**
    * @constructor
    * @param {*} object Constructs a ReportObject template from optional Flat report object
    */
   constructor(object) {

      this.user_id = object.userID || 0;
      this.username = object.username || "";
      this.time = object.time || Date.now();

      this.progress_num = object.progressNum || -1;
      this.is_completed = object.isCompleted || false;

      this.type = object.type || "未指定";
      this.comment = object.comment || "";
      this.is_compensate = object.isCompensate || false;

      this.error = object.error || ""

   }

   /**
    * Resolve `userID` and `username` from `member` then put the result inside `ReportObject`.
    * @param {object} member `member` object from discord.js
    */
   setUser(member) {
      this.user_id = member.id;
      this.username = UtilLib.extractInGameName(member.displayName, false);
   };

   /**
   * Put the parameters in `week` and `boss` into `ReportObject`.
   * @param {number} week A numeric expression for current week (Starts from 1)
   * @param {number} boss A numeric expression representing current boss (1-5)
   */
   setProgressNum(week, boss) {
      this.progress_num = parse.parse(week, boss);
   };

   /**
    * Set `is_completed` to `true`.
    */
   setComplete() {
      this.is_completed = true;
   };

   setError(inputError) {
      this.error = inputError;
   };

   /**
    * Run parameters in `ReportContentHandler.decompose` and put the result inside `ReportObject`.
    * @param {string} detail `detail` field from `.knife`
    * @param {boolean} isCompensate `isCompensate` field from `.knife`
    */
   setDetails(detail, isCompensate) {
      const RCHOutput = ReportContentHandler.decompose(detail, isCompensate);
      this.type = RCHOutput.type;
      this.comment = RCHOutput.comment;
      this.is_compensate = RCHOutput.is_compensate;
      this.error = RCHOutput.error
   };

   /**
    * Setting all fields of `ReportObject` all at once.
    * @param {object} member `member` object from discord.js
    * @param {number} week A numeric expression for current week (Starts from 1)
    * @param {number} boss A numeric expression representing current boss (1-5)
    * @param {string} detail `detail` field from `.knife`
    * @param {boolean} isCompensate `isCompensate` field from `.knife`
    */
   setAll(member, week, boss, detail, isCompensate) {
      this.setUser(member);
      this.setProgressNum(week, boss);
      this.setDetails(detail, isCompensate);
   };


   flatten() {
      let flattenedObj = {}
      for (const [key, value] of Object.entries(this)) {
         flattenedObj[key] = value
      }
      return flattenedObj
   }
}

module.exports = ReportObject;