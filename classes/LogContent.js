const strings = require("../lib/strings.json");
const { MessageEmbed } = require('discord.js');
const log = require("../lib/log-format.json");
const UtilLib = require("../api/util-lib");
const parse = require("../api/parse-progress");

class LogContent {
   /**
    * Create a new `LogContent` object. Use `LogContent.output` to get result.
    * @param {string} logFormat Format of the result 
    * @param {object} content Content that will be converted to result
    * @param {string} logType `string` or `embed`
    */
   constructor(logFormat, content = {}, type = "embed", avatarURL = null) {

      this.output;

      if (type == "string") {
         this.generateString(logFormat, content);
      }
      else if (type == "embed") {
         this.generateEmbed(logFormat, content, avatarURL);
      }
      else { throw new RangeError("Invalid logType, it must be either \"string\" or \"embed\".") }

   }

   /**
    * Create a string output and put in `this.output` for logging.
    * @param {string} command 
    * @param {object} content 
    */
   // generateString(logFormat, content) {

   //    let outputString = log[logFormat].string;
   //    const replaceType = [
   //       "week",
   //       "boss",
   //       "username",
   //       "type",
   //       "comment"
   //    ];
   //    for (let i in replaceType) { // replace indexed element in replaceType[]
   //       if (content[replaceType[i]]) {
   //          outputString = outputString.replace("%" + replaceType[i], content[replaceType[i]]);
   //       }
   //    }

   //    switch (logFormat) {
   //       case "next":
   //          if (content.pingUserStr != "") {
   //             outputString += log.next.string2 + "\n";
   //             outputString += content.pingUserStr;
   //          }
   //          break;
   //       default:
   //    };

   //    this.output = outputString;
   //    return;

   // }

   /**
    * Create an embed output and put in `this.output` for logging.
    * @param {string} command 
    * @param {object} content 
    */
   generateEmbed(logFormat, content, avatarURL) {

      let embedTitle = log[logFormat].embed.title;
      const replaceType = [
         "week",
         "boss",
         "username",
         "type",
         "comment"
      ];

      content.week = parse.unparse(content.progressNum, "week");
      content.boss = parse.unparse(content.progressNum, "boss");

      if (content.isCompensate == true) {
         embedTitle = embedTitle.replace("%compensate", "🔸");
      } else {
         embedTitle = embedTitle.replace("%compensate", "🔹");
      }

      for (let i in replaceType) { // replace indexed element in replaceType[]
         if (content[replaceType[i]]) {
            embedTitle = embedTitle.replace("%" + replaceType[i], content[replaceType[i]]);
         }
      }

      switch (logFormat) { // handles extra

         case "knifeReportCancelAll":
         case "adminKnifeReportCancelAll":

            embedTitle = embedTitle.replace("%count", content.count);

            if (!content.week) {
               embedTitle = embedTitle.replace("%wb", "所有報刀");
            }
            else if (content.week && !content.boss) {
               embedTitle = embedTitle.replace("%wb", content.week + "周");
            }
            else if (content.week && content.boss) {
               embedTitle = embedTitle.replace("%wb", content.week + "周 " + content.boss + "王")
            };
            break;

         case "report":
            embedTitle = embedTitle.replace("%hpLeft", content.hpLeft);
            break;

         default:
      }

      const date = UtilLib.getFormattedDate()
      const embed = new MessageEmbed()
         .setColor(log[logFormat].embed.color)
         .setFooter(`時間：${date}`)

      if (avatarURL) {
         embed.setAuthor(embedTitle, avatarURL);
      } else {
         embed.setTitle(embedTitle);
      }

      this.output = embed;
      return;

   }

}

module.exports = LogContent