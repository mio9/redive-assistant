const admin = require("firebase-admin");
const serviceAccount = require("../private_keys/redive-assistent-e172339968e7.json");

class Database {
  constructor() {
    admin.initializeApp({
      credential: admin.credential.cert(serviceAccount),
    });
  }
}

// module.exports = Database