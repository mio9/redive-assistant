const db = require('../classes/Database')
const ProgressManager = require('../classes/ProgressManager')
const KnifeReportManager = require('../classes/KnifeReportManager')
const InfoManager = require('../classes/InfoManager')
const SettingsManager = require('../classes/SettingsManager')
const UserManager = require('../classes/UserManager')

let pm = new ProgressManager(db)
let krm = new KnifeReportManager(db)
let im = new InfoManager(db)
let sm = new SettingsManager(db)
let um = new UserManager(db)

module.exports = {
  ProgressManager: pm,
  KnifeReportManager: krm,
  InfoManager: im,
  SettingsManager: sm,
  UserManager: um
}
