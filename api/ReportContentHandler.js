const generalizationKeys = require("../lib/generalization-keys.json");
const { MessageEmbed } = require('discord.js');
const strings = require("../lib/strings.json");
const parse = require("../api/parse-progress");

class ReportContentHandler {

   /**
    * Decomposes the detail field of a knife report from .knife into type, comment and isCompensate.
    * @param {string} detail Detail from .knife 
    * @param {boolean} isCompensate Compensate knife
    * @returns {object} Report details, using class ReportObject
    */
   static decompose(detail, isCompensate) {

      let reportObject = { error: "", type: "未指定" };

      detail = removeProblematicCharacters(detail); //do what it says

      let typeKey = generalizationKeys.type; //object of type objects
      let tagKey = generalizationKeys.tag;   //object of tag objects

      let comment;  //will get content from "detail", put into reportObject.comment later

      let typeObject;         //temp object for copying from typeKey
      let knifeTypeConverted = false;

      //conversion enabled
      //match type and put in type field

      Object.keys(typeKey).forEach(keyOfType => {
         typeObject = typeKey[keyOfType];
         /* typeObject:
         {
            "include":{
               "xxx",
               "xxx"
            },
            "name": "type",
            "id": "typeID",
            "regex": "regex"
         }                         */

         let typerx = new RegExp(typeObject.regex, 'g'); // create regex from external file
         if (typerx.test(detail)) {                      // test if contains knife type
            detail = detail.replace(typerx, "");         // replace respective knife type

            if (knifeTypeConverted == true) {
               //second match found, change type to not specified
               reportObject.error = strings.knifeContentHandler.multiple_match_found;
               reportObject.type = typeKey.noMatch.name;
            }
            else {
               reportObject.type = typeObject.name;
               knifeTypeConverted = true;
            };
         }

      });

      //no match found
      if (!knifeTypeConverted) {
         reportObject.error = strings.knifeContentHandler.no_match_found;
      };

      //end of type matching
      //rest comment handling

      //match compensate and remove tag
      let compensaterx = new RegExp(tagKey.compensate.regex, 'g');
      if (compensaterx.test(detail)) {
         detail = detail.replace(compensaterx, "");
         isCompensate = true;
      };

      //match 1 knife and removes the tag
      let onekniferx = new RegExp(tagKey.oneKnife.regex, 'g');
      if (onekniferx.test(detail)) {
         detail = detail.replace(onekniferx, "");
         reportObject.type = `${tagKey.oneKnife.name} ` + reportObject.type;
      };

      // comment = UtilLib.filter(detail, "all"); // copy detail to comment and do filtering
      // Fine you win, I give up
      // end of comment handling

      //output handling
      comment = this._truncateString(detail, 26);

      if (comment && comment != "") {
         //Auto manage damage in comment
         comment = matchAndReplaceDamage(comment);
         //Auto manage time left in comment
         comment = matchAndReplaceTimeLeft(comment);
      };

      reportObject.comment = comment;
      reportObject.is_compensate = isCompensate;

      return reportObject;
      //functions--------------------------------------------------------------------

      function removeProblematicCharacters(string) {
         return string.replace(/:(.*?):([0-9]+)|[!"$%&*#|:<>?@`' .¬~^♪]/g, "");
      };

      function matchAndReplaceDamage(string) {

         let output;

         //pulls out the damage into arr
         var rx = /((\d+-\d+)|(\d+~\d+)|\d+)(?!(s|秒|-|~|\d+))(w|W|萬|)/g;
         var arr = rx.exec(string);

         if (arr) {
            let target = arr[0];

            //pulls out the number from the damage
            var rx2 = /((\d+-\d+)|(\d+~\d+)|\d+)/g;
            var arr2 = rx2.exec(target);

            output = arr2[0] + "萬";
            string = string.replace(target, output);
         };
         return string;
      };

      function matchAndReplaceTimeLeft(string) {

         let output;

         var rx = /\d+(s|秒)/g;
         var arr = rx.exec(string);

         if (arr) {
            let target = arr[0];

            var rx2 = /\d+/g;
            var arr2 = rx2.exec(target);

            output = arr2[0] + "秒";
            string = string.replace(target, output);
         };
         return string;
      };

   };

   /**
    * Takes report data and create an embed out of it
    * @param {object} reportObject Report data inside an object 
    * @param {string} color (Hex color code) Color of the embed message. Default will match compensate type.
    * @param {string} error Error to be put into embed footer
    */
   static composeEmbed(reportObject, color = "default", error = null, avatarURL = null) {

      const outputEmbed = new MessageEmbed();

      // set outputEmbed title and description
      let embedTitle = `${reportObject.username} | `;
      embedTitle += reportObject.is_compensate ? "🔸" : "🔹";
      embedTitle += ` ${parse.textUnparseAll(reportObject.progress_num)} ${reportObject.type}`;

      if (avatarURL) {
         outputEmbed.setAuthor(embedTitle, avatarURL);
      } else {
         outputEmbed.setTitle(embedTitle);
      };

      const embedDescription = "" + this._truncateString(reportObject.comment, 20);
      outputEmbed.setDescription(embedDescription);

      //put error under outputEmbed
      if (error) {
         outputEmbed.setFooter(error);
      };

      //set color
      if (color == "default") {
         if (reportObject.is_compensate == true) {
            outputEmbed.setColor("#ffa300");
         } else {
            outputEmbed.setColor("#50a000");
         };
      }
      else outputEmbed.setColor(color);

      return outputEmbed;

   };

   //takes in string, truncate it to (length - 2) and then add "..." at the back if longer than (length)
   static _truncateString(string, truncateLength) {
      if (string.length > truncateLength) {
         return `${string.slice(0, truncateLength - 2)}...`;
      } else {
         return string;
      };
   };

};

module.exports = ReportContentHandler