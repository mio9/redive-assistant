class ParseProgress {

   /**
    * Parses Week and Boss into progressNum
    * @param {integer} week Week
    * @param {integer} boss Boss
    * @returns {integer} Progress Number
    */
   static parse(week, boss) {
      return (week - 1) * 5 + (boss - 1)
   };

   /**
    * Unparses progress ID into `week`, `boss` or `stage`
    * @param {integer} progressNum Progress Number
    * @param {string} unparseType week | boss | stage
    * @returns {integer}
    */
   static unparse(progressNum, unparseType) {
      switch (unparseType) {
         case "week":
            return parseInt(Math.floor(progressNum / 5) + 1);
            break;

         case "boss":
            return (progressNum % 5) + 1;
            break;

         case "stage":
            const curWeek = this.unparse(progressNum, "week");
            if (curWeek <= 3) return 1;
            else if (curWeek <= 10) return 2;
            else if (curWeek <= 34) return 3;
            else return 4;
            break;

         default:
            return 0;
      };
   };

   /**
    * Unparses progressNum into week, boss and current stage
    * @param {integer} progressNum Progress Number
    * @returns {object} Contains `week` `boss` and `stage`
    */
   static unparseAll(progressNum) {
      return {
         week: this.unparse(progressNum, "week"),
         boss: this.unparse(progressNum, "boss"),
         stage: this.unparse(progressNum, "stage")
      }
   }

   /**
    * Get a list of all possible progress numbers for current boss
    * @param {integer} progressNum ProgressNumber
    * @returns {array} Array of 5 progress numbers
    */
   static getWeekArray(progressNum) {
      const ID = this.parse(this.unparse(progressNum, "week"), 1);
      return [
         ID,
         ID + 1,
         ID + 2,
         ID + 3,
         ID + 4
      ]
   }

   /**
    * Get week and boss in chinese text form
    * @param {integer} progressNum Progress Number
    * @returns {string} String in format of %week周%boss王
    */
   static textUnparseAll(progressNum) {
      const unparse = this.unparseAll(progressNum);
      return `${unparse.week}周${unparse.boss}王`;
   }
}

module.exports = ParseProgress