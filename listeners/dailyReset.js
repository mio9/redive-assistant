const { Listener } = require('discord-akairo');
const { KnifeReportManager, UserManager} = require('../api/storage-lib');

class DailyResetListener extends Listener {
   constructor() {
      super('dailyReset', {
         emitter: 'client',
         event: 'dailyReset'
      });
   }

   async exec() {
      console.log("[E][Daily Reset]: --------START--------")
      const serversRef = this.client.db.collection("servers")
      const res = await serversRef.where("settings.daily_board_reset", "==", true).get()

      let resetServerId = res.docs.map(d => d.id) //array

      console.log("Targetting server: ", resetServerId)

      for (let serverId of resetServerId) {
         const guild = this.client.util.resolveGuild(serverId, this.client.guilds.cache)
         if (guild) {
            console.log(`> Clearing reports for guild: ${guild.id} (${guild.name})`)

            await KnifeReportManager.deleteAllReport(serverId);
            await UserManager.resetAllCompensate(serverId);
            this.client.emit("knifeUpdate", guild);

         } else {
            
            console.log(`> Skipping guild: ${serverId} (N/A)`)
         }

      }


      console.log("[E][Daily Reset]: ----------END--------")
   }
}

module.exports = DailyResetListener;