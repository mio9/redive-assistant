const { Listener } = require('discord-akairo')
const { InfoManager } = require('../api/storage-lib')

class BroadcastListener extends Listener {

   constructor() {
      super('broadcast', {
         emitter: 'client',
         event: 'broadcast'
      });
   };

   /**
    * Broadcast a text message or an embed message to a channel defined in settings.
    * @param {*} guild Guild Object from `discord.js`
    * @param {*} message Either a `string` message or an `embed` object from `discord.js`
    * @param {string} channel `log` (default), `chat` or `announcement` 
    */
   async exec(guild, msg, channel = "log") {

      const guildID = guild.id;

      const setting = await InfoManager.get(guildID, "settings");

      let broadcastChannel;

      switch (channel) {

         case "log":

            if (setting.log_channel) {
               broadcastChannel = this.client.util.resolveChannel(setting.log_channel, guild.channels.cache);
            } else {
               console.log(`[E][ERROR] ${guild.id} : Log Channel Missing`);
               return;
            };
            break;

         // case "chat":

         //    if (setting.chat_channel) {
         //       broadcastChannel = this.client.util.resolveChannel(setting.chat_channel, guild.channels.cache);
         //    } else {
         //       console.log(`[E][ERROR] ${guild.id} : Chat Channel Missing`);
         //       return;
         //    };
         //    break;

         // case "announcement":

         //    if (setting.announcement_channel) {
         //       broadcastChannel = this.client.util.resolveChannel(setting.announcement_channel, guild.channels.cache);
         //    } else {
         //       console.log(`[E][ERROR] ${guild.id} : Announcement Channel Missing`);
         //       return;
         //    };
         //    break;

         default:
            return;
      };

      if (broadcastChannel) {
         broadcastChannel.send(msg);
      };
      return;

   }
}

module.exports = BroadcastListener