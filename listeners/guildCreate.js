const { Listener } = require('discord-akairo');

class ReadyListener extends Listener {
  constructor() {
    super('guildCreate', {
      emitter: 'client',
      event: 'guildCreate'
    });
  }

  exec(guild) {
    console.log(`[G][Guild Invite]: ${guild.id} (${guild.name})`)
  }
}

module.exports = ReadyListener;