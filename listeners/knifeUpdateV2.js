const { Listener } = require('discord-akairo')
const UtilLib = require("../api/util-lib")
const { InfoManager } = require('../api/storage-lib')
const parse = require("../api/parse-progress");

class KnifeUpdateListener extends Listener {
   constructor() {
      super('knifeupdate', {
         emitter: 'client',
         event: 'knifeUpdate'
      });
   }

   async exec(guild) {

      const serverId = guild.id;

      // knife report path reference
      const reportQueryRef = this.client.db
         .collection("servers")
         .doc(serverId)
         .collection("reports");

      // server info path reference
      const serverQueryRef = this.client.db
         .collection("servers")
         .doc(serverId);

      const serverQuery = await serverQueryRef.get();
      if (!serverQuery.data() || !serverQuery.data().settings) {
         // server not yet init, do nothing
         return;
      }
      const serverSettings = serverQuery.data().settings

      // check if board_channel is defined
      if (!serverSettings.board_channel) {
         return;
      }
      // check if board_message is defined

      const boardChannel = this.client.util.resolveChannel(serverSettings.board_channel, guild.channels.cache);

      const tableText = await this.generateTableText(guild, reportQueryRef);

      if (!serverSettings.board_message) {
         // board message is not defined, send board message to the channel and save the id to database
         const boardMessage = await boardChannel.send(tableText);
         await serverQueryRef.update({ "settings.board_message": boardMessage.id })
         return;

      } else {
         // check if board_message message exist
         try {

            // try to fetch the board message
            const boardMessage = await boardChannel.messages.fetch(serverSettings.board_message)
            boardMessage.edit(tableText);

         } catch (e) {
            // actually catching the discord unknown message error due to the message being removed or in different channel

            const boardMessage = await boardChannel.send(tableText)
            await serverQueryRef.update({ "settings.board_message": boardMessage.id })

         }
      };

      return;


   }

   async generateTableText(guild, reportQueryRef) {

      const reportQuery = await reportQueryRef.get();

      const currentProgressNum = await InfoManager.get(guild.id, "current_progress_num");
      const parseResult = parse.unparseAll(currentProgressNum);
      const currentWeek = parseResult.week;
      const currentBoss = parseResult.boss;
      const currentStage = parseResult.stage;

      // constant server info
      const serverInfo = await InfoManager.get(guild.id);
      const bossNames = serverInfo.boss_data.boss_name;
      const bossMaxHP = serverInfo.boss_data.boss_max_hp;
      const tableDisplayWeekCount = serverInfo.settings.knife_table_display_weeks;
      const currentBossHP = serverInfo["current_boss_HP"];
      const tableDisplayBossCount = tableDisplayWeekCount * 5;
      const currentWeekStart = parse.parse(currentWeek, 1);    // starting progress number for current boss

      let queryResult = []
      reportQuery.docs.forEach(doc => {
         queryResult.push(doc.data())
      })

      /*

      const newSampleArrayData = [
         <num progress_num>      [{progress_num: current week boss 1}, {progress_num: current week boss 1}],
         <num progress_num + 1>  [{progress_num: cur. week boss 2},...{}],
         .
         <num progress_num + 4>  [{progress_num: cur. week boss 5}],
         <num progress_num + 5>  [{progress_num: next week boss 1}],
         .
         .
         <num progress_num + 5 * displayWeeks - 1> []
      ]

      */

      let reportArray = [];


      for (let i = currentWeekStart; i <= currentWeekStart + tableDisplayBossCount - 1; i++) {

         //strip out each week report and put inside reportArray
         let bossReports = queryResult.filter(obj => obj.progress_num == i);
         reportArray.push(bossReports);
      };

      let outputText = "";
      outputText += `本周：${currentWeek} | 階段：${currentStage}\n`
      outputText += `目前目標：${this.getBossChineseName(currentBoss)} - ${bossNames[currentBoss - 1]} |`
      outputText += `剩餘血量：${Math.round(10 * currentBossHP) / 10}萬 \n`

      // hp bar

      const progressPercent = currentBossHP / bossMaxHP[currentStage][currentBoss - 1];
      const progressUnits = Math.floor(25 * progressPercent);

      // slice slice slice
      outputText += `\`[${"◽◽◽◽◽◽◽◽◽◽◽◽◽◽◽◽◽◽◽◽◽◽◽◽◽◾◾◾◾◾◾◾◾◾◾◾◾◾◾◾◾◾◾◾◾◾◾◾◾◾".slice(25 - progressUnits, 50 - progressUnits)}]\` ${Math.ceil(progressPercent * 10000) / 100}%\n`;
      outputText += `🔸 補償刀 | 🔹 完整刀 | ✅ 已出刀\n`
      outputText += `\n`;

      for (let i = currentWeekStart; i <= currentWeekStart + tableDisplayBossCount - 1; i++) {

         // boss info sector
         let thisParseResult = parse.unparseAll(i);
         let thisWeek = thisParseResult.week;
         let thisBoss = thisParseResult.boss;
         let thisStage = thisParseResult.stage;


         // header
         if (i == currentWeekStart) {              // first week boss 1
            outputText += `**本周目** ━━━━━━━━━━━━━━━━━━ ${thisWeek}周\n`
         } else if (i == currentWeekStart + 5) {   // exactly 5 boss away (next week boss 1)
            outputText += `**下周目** ━━━━━━━━━━━━━━━━━━ ${thisWeek}周\n`
         } else if (i % 5 == 0) {                   // every boss 1
            outputText += `━━━━━━━━━━━━━━━━━━━━━━ ${thisWeek}周\n`
         };

         // boss info
         if (i < currentProgressNum) {       // before current boss: delete line
            outputText += "     ~~";
         } else if (i == currentProgressNum) {// current boss: arrow and bold
            outputText += "**➤ ";
         } else {                            // nothing
            outputText += "      ";
         };
         // adding boss name
         outputText += `${this.getBossChineseName(thisBoss)} ${bossNames[thisBoss - 1]} `;

         // adding the boss HP part
         if (i == currentProgressNum) {         // display current HP if current boss
            outputText += ` | ${Math.round(10 * currentBossHP) / 10} / ${bossMaxHP[thisStage][thisBoss - 1]}萬**`;
         } else {                               // 
            outputText += ` | ${bossMaxHP[thisStage][thisBoss - 1]}萬`;
         };


         if (i < currentProgressNum) {  //delete line
            outputText += "~~"
         };

         // check if this boss contains data
         if (reportArray[i - currentWeekStart]) {
            let thisArray = reportArray[i - currentWeekStart];
            thisArray.sort((a, b) => a.time - b.time);                    // sort by time
            thisArray.sort((a, b) => b.is_compensate - a.is_compensate);  // sort by compensate

            // boss loop
            for (let report of thisArray) {
               let memberName;
               // username handling
               if (!report.username) {
                  memberName = "已退出Discord";
               } else {
                  memberName = report.username;
               };
               // colored square
               if (report.is_compensate == true) {
                  outputText += "\n     🔸";
               } else {
                  outputText += "\n     🔹";
               };
               outputText += ` **${memberName}** ‖ ${report.type}`;

               //add comment if needed
               if (report.comment != "") {
                  outputText += ` (${report.comment})`;
               } else {
                  outputText += "";
               };

               // check completed
               if (report.is_completed == true) {
                  outputText += " ✅";
               } else {
                  if (report.is_compensate == true) {
                     outputText += " 🔸";
                  } else {
                     outputText += " 🔹";
                  };
               }

            };
         };

         // thin line for non-5th boss
         if ((i % 5) != 4) {
            outputText += `\n    ┈┈┈┈┈┈┈┈┈┈┈┈┈┈`;
         };
         // extra spacing for 5th boss
         if ((i % 5) == 4) {
            outputText += `\n`;
         };

         outputText += `\n`;
      };

      console.log(`[E][Board Update] ${guild.id}`);
      return outputText + `最後更新：` + UtilLib.getFormattedDate();

   };

   // convertWeekToStage(week) {

   //    //return "11122222223333333333333333333333334".slice(Math.min(week - 1, 34), Math.min(week, 35));

   //    if (week <= 3) {
   //       return 1;
   //    } else if (week >= 4 && week <= 10) {
   //       return 2;
   //    } else if (week > 10 && week <= 34) {
   //       return 3;
   //    } else if (week >= 35) {
   //       return 4;
   //    }
   //    else return 0;
   // };

   getBossChineseName(number) {
      return ("一二三四五".slice(number - 1, number) + "王");
   };

}

module.exports = KnifeUpdateListener;
