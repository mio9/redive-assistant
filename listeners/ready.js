const { Listener } = require('discord-akairo');

class ReadyListener extends Listener {
   constructor() {
      super('ready', {
         emitter: 'client',
         event: 'ready'
      });
   }

   exec() {
      console.log('Bot loading complete');
      let date = new Date()
      console.log(date.toLocaleString("ja-JP",{timeZone: "Asia/Hong_Kong",hour12:false}))

      this.client.user.setActivity('KT滑刀', {type: "WATCHING"})
      .then(presence => console.log(`Activity set to ${presence.activities[0].name}`))
      .catch(console.error);

   }
}

module.exports = ReadyListener;