const { Listener } = require('discord-akairo');
const strings = require("../lib/strings.json")

class ReadyListener extends Listener {
  constructor() {
    super('cooldown', {
      emitter: 'commandHandler',
      event: 'cooldown'
    });
  }

  exec(message, command, time) {
    let disp = strings.cooldownMaster.cooldown
    message.channel.send(disp.replace(/%time%/g, Math.ceil(time/1000) ))
  }
}

module.exports = ReadyListener;