const { Command } = require("discord-akairo");
const { MessageEmbed } = require("discord.js");
const strings = require("../../lib/strings.json");
const OpenCC = require('opencc');
const converter = new OpenCC('s2t.json');
const command = require("../../lib/admin-command-info.json");
const ReportContentHandler = require("../../api/ReportContentHandler");
const { KnifeReportManager, SettingsManager } = require('../../api/storage-lib');
const UtilLib = require("../../api/util-lib");
const Log = require("../../classes/LogContent");
const parse = require("../../api/parse-progress");

class AdminEditCommand extends Command {
   constructor() {
      super("adminedit", {
         aliases: command.commandList.adminedit.alias,
         cooldown: 5000,
         args: [
            {
               id: "user",
               type: "member",
               prompt: {
                  start: strings.prompt.user,
                  retry: strings.prompt.not_a_member,
               },
            },
            {
               id: "week",
               type: "integer",
               prompt: {
                  start: strings.prompt.week,
                  retry: strings.not_a_number,
               },
            },
            {
               id: "boss",
               type: "integer",
               prompt: {
                  start: strings.prompt.boss,
                  retry: strings.not_a_number,
               },
            },
            {
               id: "data1",
               type: "string"
            },
            {
               id: "data2",
               type: "string",
               match: "restContent"
            }
         ]
      })
   }

   async userPermissions(message) {
      return await SettingsManager.checkAdmin(message);
   };

   async exec(message, args) {

      let loadingMsg = await message.channel.send(UtilLib.randomLoading());

      const serverDocRef = this.client.db
         .collection("servers")
         .doc(message.guild.id)
      const serverDoc = await serverDocRef.get()

      // query
      const targetProgressNum = parse.parse(args.week, args.boss);

      const reportQuery = {
         progress_num: targetProgressNum,
         user_id: args.user.user.id
      };
      if (!args.data1) {
         args.data1 = ""
      }

      // get avatar
      const avatarURL = args.user.user.displayAvatarURL();

      // input handling
      let newReportObj = {};
      let editCompensate = false;

      if (!isNaN(parseInt(args.data1)) && !isNaN(parseInt(args.data2)) && args.data1 <= 99) {
         // data1 and data2 are numbers => change week and boss

         const newWeek = parseInt(args.data1);
         const newBoss = parseInt(args.data2);
         // index = args.data3;

         //missing week
         if (isNaN(newWeek)) {
            return loadingMsg.edit(`${strings.edit.r.week_missing}\n${strings.edit.r.usage}`);
         };
         //missing boss
         if (isNaN(newBoss)) {
            return loadingMsg.edit(`${strings.edit.r.boss_missing}\n${strings.edit.r.usage}`);
         };

         newReportObj.progress_num = parse.parse(newWeek, newBoss);

         //can eslint stop shitting on me pls

         const currentProgress = serverDoc.data().current_progress_num;
         const currentProgressStart = parse.getWeekArray(currentProgress)[0]; // boss 1 of corrent week

         // check within range
         if (newReportObj.progress_num < currentProgress ||  //cannot report before
            newReportObj.progress_num > currentProgressStart + 4 + serverDoc.data().settings.user_max_future_knife_weeks * 5) {
            // cannot report beyond 

            return loadingMsg.edit(strings.knifeReport.out_of_range);
         };

      } // end of week boss edit


      else if (/\b(c|C)\b/.test(args.data1)) {           // data 1 is ONLY c/C, or seperated by symbol => compensate toggle
         editCompensate = true;                          // use for laters
      }                                                  // end of compensate toggle


      else {                                             // no match => detail edit
         let detail = String(args.data1)
         if (args.data2) detail += String(args.data2);   // add data2 to the end of data1 since it is type edit

         if (detail) {
            detail = await converter.convertPromise(detail);
            detail = detail.replace(/( |^)(-|)(c|C)/g, "")
         } else {
            detail = "";                                 // add empty space to prevent error
         };

         const RCHOutput = ReportContentHandler.decompose(detail, false);

         newReportObj.type = RCHOutput.type;
         newReportObj.comment = RCHOutput.comment;
         newReportObj.error = RCHOutput.error;
      };

      newReportObj.time = Date.now();

      // handle multiple cases
      // get reports
      const reportArr = await KnifeReportManager.getReports(message.guild.id, reportQuery, "asc");
      let selectedReport;
      // no record found
      if (reportArr.length == 0) {
         return loadingMsg.edit(strings.knifeReport.no_record);
      }

      // exactly 1 match found
      else if (reportArr.length == 1) {
         // set ID as the ID of first report in array
         selectedReport = reportArr[0];
      }

      // more than 1 match
      else if (reportArr.length >= 2) {

         loadingMsg.edit(strings.edit.multiple_match_found);

         // display available choices
         const listEmbed = new MessageEmbed()
            .setColor("#F0F0F0")
            .setAuthor(`${reportArr[0].data.username} | ${parse.textUnparseAll(targetProgressNum)}`, avatarURL);

         // loop data
         for (let i in reportArr) {
            let reportData = reportArr[i].data;

            const type = reportData.type;
            const comment = reportData.comment;

            const date = new Date(reportData.time);
            const time = UtilLib.formatDate(date);

            const thisReportTitle = reportData.is_compensate ? `${parseInt(i) + 1}| 🔸 ${type}` : `${parseInt(i) + 1} |🔹 ${type}`;
            const thisReportDetail = comment ? `備註：${comment}\n時間：${time}` : `時間：${time}`;

            listEmbed.addField(thisReportTitle, thisReportDetail);
         };
         // edit loading message to embed
         loadingMsg.edit(listEmbed);

         // add reactions for selection, reacts on the embed
         const selectionReturn = await UtilLib.emojiResponseSelection(args.user.user.id, loadingMsg, reportArr.length);

         if (selectionReturn == false) {  // handles selection timeout
            return;
         };

         selectedReport = reportArr[selectionReturn - 1];

      };

      // report to be edited with report ID selectedReportID

      // treat compensate problem
      if (editCompensate == true) {
         const maxKnife = serverDoc.data().settings.user_max_knife;
         const maxCompensate = serverDoc.data().settings.user_max_compensate_knife;
         const composeCompensateReturn = await this._composeCompensate(
            selectedReport, args.user.user.id, message.guild.id, maxKnife, maxCompensate
         );
         if (typeof (composeCompensateReturn) == "string") {

            loadingMsg.delete();
            message.channel.send(composeCompensateReturn);
            return;

         } else {
            newReportObj = composeCompensateReturn;
         };
      };

      const editedReport = await KnifeReportManager.edit(message.guild.id, selectedReport.id, newReportObj);
      const returnEmbed = ReportContentHandler.composeEmbed(editedReport, "#f0d060", null, avatarURL);

      loadingMsg.edit(strings.edit.success);
      loadingMsg.edit(returnEmbed);
      this.client.emit("knifeUpdate", message.guild);

      const content = {
         progressNum: editedReport.progress_num,
         isCompensate: editedReport.is_compensate,
         type: editedReport.type,
         username: editedReport.username
      };

      const log = new Log("adminEdit", content, "embed", avatarURL);
      this.client.emit("broadcast", message.guild, log.output, "log");

      return;
   };

   async _composeCompensate(report, userID, guildID, maxKnife, maxCompensate) {
      let query = {
         user_id: userID,
      };
      let reportArr = [];
      let editReportObj = {}

      if (report.data.is_compensate == true) { // original report is compensate
         query.is_compensate = false;
         reportArr = await KnifeReportManager.getReports(guildID, query);
         // get array of non-compensate reports
         // check array length
         if (reportArr.length >= maxKnife) {
            return strings.knifeReport.knife_limit;
            // return if normal knife reach limit
         };
         editReportObj = { is_compensate: false }; // set normal
      }

      else { // original report is NOT compensate
         query.is_compensate = true;
         reportArr = await KnifeReportManager.getReports(guildID, query);
         // get array of compensate reports
         // check array length
         if (reportArr.length >= maxCompensate) {
            return strings.knifeReport.compensate_limit;
            // return if compensate knife reach limit
         };
         editReportObj = { is_compensate: true };  // set compensate
      };

      return editReportObj;

   };
};



module.exports = AdminEditCommand;