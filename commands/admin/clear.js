const { Command } = require('discord-akairo');
const strings = require('../../lib/strings.json');
const command = require("../../lib/admin-command-info.json");
const { KnifeReportManager, UserManager, SettingsManager } = require('../../api/storage-lib');
const UtilLib = require("../../api/util-lib")

class ClearCommand extends Command {
   constructor() {
      super("clear", {
         aliases: command.commandList.clear.alias,
         cooldown: 5000,
         channel: "guild",
         args: [
            {
               id: "user",
               type: "member",
               prompt: {
                  start: strings.prompt.user,
                  retry: strings.prompt.not_a_member,
               },
            }
         ]
      });
   };

   async userPermissions(message) {
      return await SettingsManager.checkAdmin(message);
   };

   async exec(message, args) {

      let loadingMsg = await message.channel.send(UtilLib.randomLoading());

      await KnifeReportManager.deleteAllUserReport(message.guild.id, args.user.user.id);
      await UserManager.compensateEdit(message.guild.id, args.user.user.id, "false");

      this.client.emit("knifeUpdate", message.guild);

      // knife report complete
      return loadingMsg.edit(strings.clear.clear_complete.replace("%userid", args.user.user.id));

   };
};


module.exports = ClearCommand;
