const { Command } = require("discord-akairo");
const { Permissions, MessageEmbed } = require('discord.js');
const doc = require("../../lib/setup-doc.json");

// Initialize necessary data for the server, including next 5 weeks and 5 bosses in those weeks
// will reset everything if there's already data
class SetupCommand extends Command {
   constructor() {
      super("setup", {
         aliases: ["setup"],
         channel: "guild",
         userPermissions: Permissions.FLAGS.ADMINISTRATOR,
         args: [
            {
               id: "page_number",
               type: "integer",
               default: 1
            }
         ]
      });
   };

   exec(message, args) {

      const embed = new MessageEmbed();

      if (args.page_number < 1 || args.page_number > doc.setup_doc.length + 1) {
         return;
      };

      const arrIndex = args.page_number - 1;

      const title = doc.setup_doc[arrIndex].title;
      const contentArr = doc.setup_doc[arrIndex].content;
      let content = "";
      for (let i in contentArr) {
         content += contentArr[i];
         content += "\n"
      };

      embed.setTitle(title);
      embed.setDescription(content);

      return message.channel.send(embed);

   };

};
module.exports = SetupCommand;
