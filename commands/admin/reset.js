const { Permissions } = require("discord.js");
const { Command } = require('discord-akairo');
const strings = require('../../lib/strings.json');
const command = require("../../lib/admin-command-info.json");
const { KnifeReportManager, ProgressManager, UserManager } = require('../../api/storage-lib');
const UtilLib = require("../../api/util-lib")

class ResetCommand extends Command {
   constructor() {
      super("resetknife", {
         aliases: command.commandList.reset.alias,
         cooldown: 60000,
         userPermissions: Permissions.FLAGS.ADMINISTRATOR,
         channel: "guild",
         args: [
            {
               id: "confirmation",
               type: "string",
               prompt: {
                  start: strings.reset.confirm
               },
            }
         ]
      });
   };

   async exec(message, args) {

      if (args.confirmation != message.guild.name) {
         return message.channel.send(strings.reset.cancel);
      };

      const rand = Math.floor(Math.random() * 6);
      let loadingMsg;
      let resetto = false;
      if (rand == 0) {
         loadingMsg = await message.channel.send("⌛ 春埼、リセット");
         resetto = true;
      } else {
         loadingMsg = await message.channel.send(UtilLib.randomLoading());
      };

      await ProgressManager.resetProgress(message.guild.id);
      await KnifeReportManager.deleteAllReport(message.guild.id);
      await UserManager.resetAllCompensate(message.guild.id);

      this.client.emit("knifeUpdate", message.guild);

      // knife report complete
      if (resetto == true) {
         return loadingMsg.edit("✅ 似乎重啟了。");
      } else {
         return loadingMsg.edit(strings.reset.reset_success);
      }

   };
};


module.exports = ResetCommand;
