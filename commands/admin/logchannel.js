const { Command } = require("discord-akairo");
const strings = require('../../lib/strings.json');
const command = require("../../lib/admin-command-info.json");
const UtilLib = require("../../api/util-lib")
const { Permissions } = require('discord.js');


class LogChannelCommand extends Command {
   constructor() {
      super("logchannel", {
         aliases: command.commandList.logchannel.alias,
         cooldown: 3000,
         channel: "guild",
         userPermissions: Permissions.FLAGS.ADMINISTRATOR,
         args: [
            {
               id: "channel",
               type: "textChannel",
            }
         ]
      })
   };

   async exec(message, args) {

      let loadingMsg = await message.channel.send(UtilLib.randomLoading());

      const logchannel = args.channel;
      const guildID = message.guild.id;

      const serverQueryRef = this.client.db
         .collection("servers")
         .doc(guildID);

      if (args.channel) {
         // set specific channel as log channel

         await serverQueryRef.update({ "settings.log_channel": logchannel.id });
         loadingMsg.edit(strings.log.setup.replace("%channel", `<#${logchannel.id}>`));

      } else {

         // set current channel as log channel
         await serverQueryRef.update({ "settings.log_channel": message.channel.id });
         loadingMsg.edit(strings.log.setup_this);
      };

      return;
   };
};


module.exports = LogChannelCommand;
