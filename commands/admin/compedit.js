const { Command } = require("discord-akairo");
const { UserManager, SettingsManager } = require('../../api/storage-lib');
const UtilLib = require("../../api/util-lib");
const strings = require("../../lib/strings.json");
const command = require("../../lib/admin-command-info.json")

class CompEditCommand extends Command {
   constructor() {
      super("compedit", {
         aliases: command.commandList.compedit.alias,
         args: [
            {
               id: "user",
               type: "member",
               prompt: {
                  start: strings.prompt.user,
                  retry: strings.prompt.not_a_member,
               },
            },
            {
               id: "key",
               type: "string",
            }
         ],
      });
   };

   async userPermissions(message) {
      return await SettingsManager.checkAdmin(message);
   };

   async exec(message, args) {

      let loadingMsg = await message.channel.send(UtilLib.randomLoading());
      let returnCompensate;

      switch (args.key) {
         case "n":
         case "false":
         case "f":
            returnCompensate = await UserManager.compensateEdit(message.guild.id, args.user.user.id, "false");
            break;

         case "c":
         case "true":
         case "t":
            returnCompensate = await UserManager.compensateEdit(message.guild.id, args.user.user.id, "true");
            break;
         default:
            return;
      };

      loadingMsg.edit(strings.compStatus.edit_complete.replace("%status", JSON.stringify(returnCompensate)));


   }
}
module.exports = CompEditCommand