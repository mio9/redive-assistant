const { Command } = require("discord-akairo");
const strings = require("../../lib/strings.json");
const command = require("../../lib/admin-command-info.json");
const { KnifeReportManager, SettingsManager } = require('../../api/storage-lib');
const UtilLib = require("../../api/util-lib");
const ReportContentHandler = require("../../api/ReportContentHandler");
const { MessageEmbed } = require("discord.js");
const Log = require("../../classes/LogContent");
const parse = require("../../api/parse-progress");

class AdminCancelCommand extends Command {
   constructor() {
      super("admincancel", {
         aliases: command.commandList.admincancel.alias,
         args: [
            { id: "all", match: "flag", flag: command.flags.selectAll },
            {
               id: "user",
               type: "member",
               prompt: {
                  start: strings.prompt.user,
                  retry: strings.prompt.not_a_member,
               },
            },
            {
               id: "week",
               type: "integer",
               prompt: {
                  optional: true,
                  start: strings.prompt.week,
                  retry: strings.prompt.not_a_number,
               },
            },
            {
               id: "boss",
               type: "integer",
               prompt: {
                  optional: true,
                  start: strings.prompt.boss,
                  retry: strings.prompt.not_a_number,
               },
            },
         ]
      });
   };

   async userPermissions(message) {
      return await SettingsManager.checkAdmin(message);
   };
   
   async exec(message, args) {

      let loadingMsg = await message.channel.send(UtilLib.randomLoading());

      const deleteProgressNum = (args.week && args.boss) ? parse.parse(args.week, args.boss) : null;

      let reportQuery = {
         user_id: `${args.user.user.id}`
      };
      if (deleteProgressNum) reportQuery.progress_num = deleteProgressNum;

      const reportArr = await KnifeReportManager.getReports(message.guild.id, reportQuery);

      // zero length: return no record
      if (reportArr.length == 0) {
         return loadingMsg.edit(strings.knifeReport.no_record);
      };

      // always get first report for reference
      const deletedReport = reportArr[0].data;
      // message.channel.send(`\`\`\`${JSON.stringify(deletedReport)}\`\`\``);

      // user avatar
      const avatarURL = args.user.user.displayAvatarURL();

      let embed, statusString;
      if (args.all == false) {            // delete only first report in array
         await KnifeReportManager.delete(message.guild.id, [reportArr[0]]);
         embed = ReportContentHandler.composeEmbed(deletedReport, "#700030", null, avatarURL);
         statusString = strings.knifeReport.cancel_success;
      }
      else if (args.all == true) {        // delete all report in array
         await KnifeReportManager.delete(message.guild.id, reportArr);
         statusString = strings.knifeReport.cancel_all_success;
         //compose feedback embed
         embed = new MessageEmbed()
            .setAuthor(`${deletedReport.username} | ${reportArr.length}筆記錄`, message.author.displayAvatarURL());

         if (deleteProgressNum >= 0) {
            embed.setDescription(`無指定周目 (所有報刀)`);
         } else {
            embed.setDescription(`指定目標: ${args.week}周${args.boss}王`);
         };

      } else {
         return loadingMsg.edit(strings.error.error);
      };

      embed.setFooter(strings.knifeReport.delete_embed_footer);
      loadingMsg.edit(embed);
      loadingMsg.edit(statusString);

      this.client.emit("knifeUpdate", message.guild);

      const member = await message.guild.members.fetch(args.user.id)
      const username = UtilLib.extractInGameName(member.displayName, true);

      let content = {
         username: username,
         isCompensate: deletedReport.is_compensate
      };
      let cancelType;

      if (args.all == true) { // cancel ALL

         if (args.week && args.boss) content.progressNum = parse.parse(args.week, args.boss);
         content.count = reportArr.length;
         cancelType = "adminKnifeReportCancelAll"

      } else { // cancel 1

         content.progressNum = deletedReport.progress_num;
         content.type = deletedReport.type;
         cancelType = "adminKnifeReportCancel"

      };

      const log = new Log(cancelType, content, "embed", avatarURL);
      this.client.emit("broadcast", message.guild, log.output, "log")

      return;
   };

};

module.exports = AdminCancelCommand