const { Command } = require("discord-akairo");
const { Permissions } = require('discord.js');
const strings = require("../../lib/strings.json");
const command = require("../../lib/admin-command-info.json");
const { KnifeReportManager, ProgressManager } = require('../../api/storage-lib');

// Initialize necessary data for the server, including next 5 weeks and 5 bosses in those weeks
// will reset everything if there's already data
class InitCommand extends Command {
   constructor() {
      super("init", {
         aliases: command.commandList.init.alias,
         channel: "guild",
         userPermissions: Permissions.FLAGS.ADMINISTRATOR,
         args: [
            {
               id: "confirmation",
               type: "string",
               prompt: {
                  start: strings.reset.full_reset_confirm
               },
            }
         ]
      });
   };

   async exec(message, args) {

      if (args.confirmation != message.guild.name) {

         return message.channel.send(strings.reset.cancel);

      } else {

         // init the server default settings data
         const newData = require("../../lib/init-data.json");

         const batch = this.client.db.batch();

         await batch.commit();

         // write server settings back to main db
         await this.client.db.collection("servers").doc(message.guild.id).set(newData);

         //remove all current knife reports
         await KnifeReportManager.deleteAllReport(message.guild.id);
         //delete all existing boss info
         await ProgressManager.resetProgress(message.guild.id);

         // get update from global values
         const dataSnapshot = await this.client.db
            .collection("global-value")
            .doc("boss_data")
            .get();

         const data = dataSnapshot.data();
         const updateData = {
            boss_data: data
         };
         await this.client.db
            .collection("servers")
            .doc(message.guild.id)
            .update(updateData);

      };

      this.client.emit("knifeUpdate", message);

      return message.channel.send(strings.reset.full_reset_success);

   };

};
module.exports = InitCommand;
