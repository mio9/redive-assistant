const { Command } = require("discord-akairo");
const { Permissions } = require("discord.js");
const UtilLib = require("../../api/util-lib");
const strings = require("../../lib/strings.json");
const command = require("../../lib/admin-command-info.json");

class UpdateDataCommand extends Command {
   constructor() {
      super("updatedata", {
         aliases: command.commandList.updatedata.alias,
         cooldown: 60000,
         userPermissions: Permissions.FLAGS.ADMINISTRATOR,
      });
   };

   async exec(message) {

      let loadingMsg = await message.channel.send(UtilLib.randomLoading());

      // get update from global values
      const dataSnapshot = await this.client.db
         .collection("global-value")
         .doc("boss_data")
         .get();

      const data = dataSnapshot.data();

      const updateData = {
         boss_data: data
      };

      await this.client.db
         .collection("servers")
         .doc(message.guild.id)
         .update(updateData);

      return loadingMsg.edit(strings.update_data.success)

   };
};

module.exports = UpdateDataCommand;