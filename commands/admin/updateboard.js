const { Command } = require("discord-akairo");
const { SettingsManager } = require ('../../api/storage-lib');
const strings = require('../../lib/strings.json');
const command = require("../../lib/admin-command-info.json");

class UpdateBoardCommand extends Command {
   constructor() {
      super("updateboard", {
         aliases: command.commandList.updateboard.alias,
         cooldown: 10000,
         channel: "guild"
      });
   };
   
   async userPermissions(message) {
      return await SettingsManager.checkAdmin(message);
   };

   async exec(message) {

      this.client.emit("knifeUpdate", message.guild);
      return message.channel.send(strings.board.update);

   };
};


module.exports = UpdateBoardCommand;
