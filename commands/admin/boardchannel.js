const { Command } = require("discord-akairo");
const strings = require('../../lib/strings.json');
const command = require("../../lib/admin-command-info.json");
const utilLib = require("../../api/util-lib");
const { Permissions } = require('discord.js');

class BoardChannelCommand extends Command {
   constructor() {
      super("boardchannel", {
         aliases: command.commandList.boardchannel.alias,
         cooldown: 3000,
         channel: "guild",
         userPermissions: Permissions.FLAGS.ADMINISTRATOR,
         args: [
            {
               id: "channel",
               type: "textChannel",
            }
         ]
      })
   };

   async exec(message, args) {

      let loadingMsg = await message.channel.send(utilLib.randomLoading());

      const boardchannel = args.channel;
      const guildID = message.guild.id;

      const serverQueryRef = this.client.db
         .collection("servers")
         .doc(guildID);

      if (args.channel) {
         // set specific channel as board channel

         await serverQueryRef.update({ "settings.board_channel": boardchannel.id });
         loadingMsg.edit(strings.board.setup.replace("%channel", `<#${boardchannel.id}>`));

      } else {

         // set current channel as board channel
         await serverQueryRef.update({ "settings.board_channel": message.channel.id });
         loadingMsg.edit(strings.board.setup_this);
      };

      this.client.emit("knifeUpdate", message.guild);
      return;
   };
};


module.exports = BoardChannelCommand;
