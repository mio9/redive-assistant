const { Command } = require("discord-akairo");
const { MessageEmbed } = require('discord.js')
const command = require("../../lib/admin-command-info.json");
const { Permissions } = require("discord.js");
const setting = require("../../lib/settings-info.json");
const UtilLib = require("../../api/util-lib");

class SettingCommand extends Command {
   constructor() {
      super("setting", {
         aliases: command.commandList.settings.alias,
         userPermissions: Permissions.FLAGS.ADMINISTRATOR,
         args: [
            {
               id: "key",
               type: "string",
            },
            {
               id: "value",
               type: "string",
            },
         ],
      });
   };

   async exec(message, args) {

      let settingKey = args.key;
      let value = args.value;

      let databaseSetting = await this.client.settings.getAll(message.guild.id);
      const embed = new MessageEmbed();
      embed.setColor("#b023e8");

      let loadingMsg = await message.channel.send(UtilLib.randomLoading());

      //embed for current setting
      if (!settingKey) {
         embed.setTitle(setting.display.title);

         Object.keys(setting.list).forEach(key => {
            let embedSettingDisplay;
            if (!databaseSetting[key]) {
               embedSettingDisplay = `未設定`;
            } else {
               embedSettingDisplay = valueToDisplayFormat(setting.list[key].key, databaseSetting[key]);
            };
            embed.addField(setting.list[key].message, embedSettingDisplay, true);
         });
      }
      else {

         // list of setting keys, DOES NOT USE EMBED, RETURN DIRECTLY
         if (settingKey == "list") {
            // start
            let displayList = "```";
            // each field
            Object.keys(setting.list).forEach(key => {
               displayList += `\n${setting.list[key].message.slice(1)} | 設定碼："${setting.list[key].key}"\n${setting.list[key].description} | 類型：${setting.list[key].typeDisplay}\n`;
            });
            // end
            displayList += `\`\`\`\n${setting.display.list_end}`;
            return loadingMsg.edit(displayList);
         };


         // no value: returns embed for detail
         // strip out corresponding settings.list.[command]
         let settingObject; // object for setting corresponding to the key 
         Object.keys(setting.list).forEach(key => {
            // match corresponding setting
            if (setting.list[key].key == settingKey) {
               settingObject = setting.list[key];
            };

         });
         // handle empty object
         if (!settingObject) {
            return loadingMsg.edit(`${setting.display.invalidKey} ${setting.display.generalError}`);
         };
         // display current value
         if (!value) {
            embed.setTitle(settingObject.key);
            embed.addField(`內容`, settingObject.description, false);
            embed.addField(`範圍`, settingObject.typeDisplay, true);
            embed.addField(`目前值`, valueToDisplayFormat(settingObject.key, databaseSetting[settingObject.databaseKey]), true);
         }

         // do actual setting
         else {

            let settingDataType = settingObject.type;
            let rangeError = `${setting.display.invalidRange} ${setting.display.generalError}`;
            let dataTypeError = `${setting.display.invalidDataType} ${setting.display.generalError}`;

            // trim excess stuff, keeping ID only
            value = value.replace(/[<>#@&]/g, '');

            switch (args.key) {
               case "member":
               case "admin":
                  if ((args.value == "clear" || args.value == "null")
                     && (args.key == "member" || args.key == "admin")) {   // reset roles
                     value = "";
                  }
            }



            //check within range
            switch (settingDataType) {

               case "integer":
                  value = Number(value);
                  //invalid data type
                  if (isNaN(value)) {
                     return loadingMsg.edit(dataTypeError);
                  };
                  //outside range
                  if (value < settingObject.range[0] || value > settingObject.range[1]) {
                     return loadingMsg.edit(rangeError);
                  };
                  //make sure that it is an integer
                  value = Math.floor(value);
                  break;

               // rubbish boolean converter
               case "boolean":
                  value = stringToBoolean(value);
                  //throw out the error
                  if (typeof value != "boolean") {
                     return loadingMsg.edit(value);
                  };
                  break

               default:
            };

            // update database
            await this.client.settings.update(message.guild.id, settingObject.databaseKey, value);

            //update local settings cache
            let newDatabaseSetting = await this.client.settings.getAll(message.guild.id);
            this.client.cachedb.update({ _id: message.guild.id }, { $set: newDatabaseSetting });

            // display
            embed.setTitle(setting.display.updateSuccess);

            let displayValue = valueToDisplayFormat(settingKey, value);
            embed.setDescription(`${settingObject.message.substring(1)}：${displayValue}`);
         };
      };

      loadingMsg.edit(embed);
      return loadingMsg.edit("");



      //convert setting value to display format, uses setting.list.[databaseKey].key and args.value
      function valueToDisplayFormat(settingKey, input) {
         let output;
         switch (settingKey) {
            case "prefix":
               output = `\`" ${input} "\``;
               break;

            // case "startbattleCooldown":
            // case "nextbossCooldown":
            // case "prevbossCooldown":

            //    output = input + "秒";
            //    break;

            case "rReset":
               if (input == true || input == "true") {
                  output = ":white_check_mark:開啟";
               } else { output = ":x:關閉" };
               break;

            case "board":
            case "log":
               // case "chat":
               // case "announcement":
               output = `<#${input}>`;
               break;

            case "admin":
            case "member":
               if (input != "") {
                  output = `<@&${input}>`;
               } else { output = "未設定" }

               break;

            default:
               output = input;
         }
         return output;
      };

      //does what it says, but smarter, at least it wont convert "false" to true, which I think is dumb as f
      function stringToBoolean(input) {
         let output;
         switch (input) {
            case "true":
            case "1":
               output = true;
               break;

            case "false":
            case "0":
               output = false;
               break;

            default:
               output = `${setting.display.invalidRange} ${setting.display.generalError}`;
         };
         return output;
      };
   };
};

module.exports = SettingCommand;
