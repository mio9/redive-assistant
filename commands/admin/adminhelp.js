const { Command } = require("discord-akairo");
const { MessageEmbed } = require('discord.js');
const commandInfo = require("../../lib/admin-command-info.json");
const strings = require("../../lib/strings.json");
const UtilLib = require("../../api/util-lib");
const { SettingsManager } = require("../../api/storage-lib")

//only this command and help.js imports command-info.json or admin-command-info.json as "commandInfo". ALL other command uses "command"

class AdminHelpCommand extends Command {
   constructor() {
      super("adminhelp", {
         aliases: ["a", "admin", "ah", "ahelp", "acmd", "acommand"],
         args: [
            {
               id: "commandName",
               type: "rest"
            }
         ]
      });
   }

   async userPermissions(message) {
      return await SettingsManager.checkAdmin(message);
   };

   async exec(message, args) {

      let loadingMsg = await message.channel.send(UtilLib.randomLoading());

      const embed = new MessageEmbed();
      embed.setColor("#ffb090");
      const command = commandInfo.commandList;

      if (!args.commandName) {   // nothing in parameter

         embed.setTitle(strings.generalHelp.list_title_admin);
         embed.setFooter(strings.generalHelp.list_footer + `\n` + strings.generalHelp.admin_list_footer);

         //list of command
         Object.keys(command).forEach(key => {
            embed.addField(command[key].name, command[key].shortDescription, true);
         });

      } else {
         // match command name and return
         const commandSelected = this._getCommand(command, args.commandName);
         // handle invalid command
         if (commandSelected == false) {
            return loadingMsg.edit(strings.generalHelp.invalid_command_name);
         };

         embed.setTitle(`${commandSelected.name}: ${commandSelected.shortDescription}`);
         embed.addField("接受名稱", `\`${commandSelected.displayAlias}\``, true);
         embed.addField("指令語法", `\`${commandSelected.syntax}\``, true);
         embed.addField("用途", commandSelected.description, false);
         if (commandSelected.example) {
            embed.addField("使用範例", commandSelected.example, false);
         };
         embed.setFooter(strings.generalHelp.detail_footer)

      };

      loadingMsg.delete();
      return message.channel.send(embed)
   };

   _getCommand(command, commandName) {
      let isMatched = false;
      let returnObj;
      Object.keys(command).forEach(key => {
         if (command[key].alias.includes(commandName) && isMatched == false) {
            returnObj = command[key];
            isMatched = true;
         };
      });
      if (returnObj) {
         return returnObj;
      } else return false;
   };
};

module.exports = AdminHelpCommand;