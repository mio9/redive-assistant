const { Command } = require("discord-akairo");
const strings = require("../../lib/strings.json");
const { MessageEmbed } = require('discord.js');
const command = require("../../lib/command-info.json");

class CalculateCommand extends Command {

   constructor() {
      super("calculate", {
         aliases: command.commandList.calculate.alias,
         args: [
            {
               id: "hpLeft",
               type: "number"
            },
            {
               id: "knifeDamage1",
               type: "number"
            },
            {
               id: "knifeDamage2",
               type: "number"
            },
            {
               id: "knifeDamage3",
               type: "number"
            },
            {
               id: "knifeDamage4",
               type: "number"
            },
            {
               id: "knifeDamage5",
               type: "number"
            },
            {
               id: "noSorting",
               match: "flag",
               flag: ["-nosort", "-ns", "nosort", "ns"]
            }
         ],
      });
   };

   exec(message, args) {

      const embed = new MessageEmbed();
      embed.setColor("#90ffff");

      let hpLeft = args.hpLeft;
      let damage = { d1: 0, d2: 0, d3: 0, d4: 0, d5: 0 };

      damage.d1 = args.knifeDamage1;
      damage.d2 = args.knifeDamage2;
      if (args.knifeDamage3) { damage.d3 = args.knifeDamage3 };
      if (args.knifeDamage4) { damage.d4 = args.knifeDamage4 };
      if (args.knifeDamage5) { damage.d5 = args.knifeDamage5 };

      //handle rubbish input for hpLeft
      if (hpLeft && ((hpLeft <= 0) || (hpLeft > 3000))) {
         // !(hp > 0 && hp <= 3000)
         return message.channel.send(strings.calculate.out_of_range);
      };

      if ((damage.d1 && (damage.d1 < 0)) || ((damage.d2 && (damage.d2 < 0)))) {
         //negative damage
         return message.channel.send(strings.calculate.damage_negative);
      };

      if ((damage.d1 && (damage.d1 > 3000)) || ((damage.d2 && (damage.d2 > 3000)))) {
         //damage higher than range
         return message.channel.send(strings.calculate.damage_too_high);
      };


      //check arg
      let arg1 = hpLeft ? 1 : 0;
      let arg2 = damage.d1 ? 2 : 0;
      let arg3 = damage.d2 ? 4 : 0;

      let arg = arg1 + arg2 + arg3;


      let timeLeft, requiredDamage;

      switch (arg) {
         case 1:
            requiredDamage = Math.ceil(4.3 * hpLeft / 5.3);

            embed.setTitle(`兩刀完全相同全返所需傷害`);
            embed.setDescription(`剩餘血量： ${hpLeft}萬\n以常用4.3倍計算:`);
            embed.addField(`所需傷害: ${requiredDamage}萬`, strings.calculate.max_damage_notice);
            embed.setFooter(strings.calculate.output_footer);

            break;

         case 3:
            requiredDamage = Math.ceil(Math.max((hpLeft - damage.d1), 0) * 4.3);

            embed.setTitle(`第二刀獲得全返所需傷害`);
            embed.setDescription(`剩餘血量： ${hpLeft}萬\n第一刀傷害： ${damage.d1}萬`);
            if (requiredDamage >= hpLeft) {
               //require damage higher than hp left
               embed.addField(strings.calculate.cp_greater_than_health, strings.calculate.full_return_impossible);
            }
            else {
               if (damage.d1 >= hpLeft) {
                  //first knife damage higher than hp left
                  embed.addField(strings.calculate.full_return_impossible, strings.calculate.general_excess_damage);
               }
               else {
                  //normal
                  embed.addField(`所需第二刀傷害: ${requiredDamage}萬`, strings.calculate.max_damage_notice);
                  embed.setFooter(strings.calculate.output_footer);
               };
            };
            break;

         //more than 1 damage input
         //redo

         case 7:

            embed.setTitle(`補償刀剩餘時間`);

            let damageArr = [];
            let damageArrIndex = 0;
            //putting all input values that are in range into damageArr[]
            for (const dn in damage) {

               if (!(!damage[dn] || damage[dn] <= 0)) { //check if damage is not empty and valid
                  damageArr[damageArrIndex] = Math.min(damage[dn], hpLeft);
                  damageArrIndex++;
               };
            };
            //sort in descending order
            if (args.noSorting == false) {
               damageArr.sort((a, b) => b - a)
            };


            //display damages
            let embedDescription = `剩餘血量： ${hpLeft}萬\n`;
            let dispIndex = 0;
            let damageSum = 0;


            //putting each value in array into description string
            damageArr.forEach(thisDamage => {
               if (hpLeft > damageSum) {
                  embedDescription += ("第" + "一二三四五".slice(dispIndex, dispIndex + 1) + "刀傷害: "); //output each 
                  embedDescription += (`${thisDamage}萬\n`);
                  dispIndex++;
                  damageSum += thisDamage;
               };
            });
            dispIndex--;
            //dispIndex 
            embed.setDescription(embedDescription);

            if (dispIndex == 0) { //first damage already in excess
               embed.addField(strings.calculate.full_return_impossible, strings.calculate.general_excess_damage);
               break;
            } 
            else {

               //reduce: sum of array
               if (damageArr.reduce((a, b) => a + b) <= hpLeft) {
                  //damage too low
                  embed.addField(strings.calculate.damage_too_low, strings.calculate.full_return_impossible);
               }
               else {
                  damageSum -= damageArr[dispIndex];
                  timeLeft = Math.min(Math.max(Math.ceil((1 - ((hpLeft - damageSum) / damageArr[dispIndex])) * 90 + 20), 0), 90);
                  let timeField;
                  if (args.noSorting == true) {
                     timeField = strings.calculate.arrange_damage_notice;
                  } else {
                     timeField = strings.calculate.max_damage_notice;
                  };
                  embed.addField(`剩餘時間: ${Math.floor(timeLeft / 60)}分 ${timeLeft % 60}秒`, timeField);

               };
            };

            break;

         default:
            //return error: wrong argument
            return message.channel.send(strings.error.invalid_input_number);

      };
      return message.channel.send(embed);

   };
};

module.exports = CalculateCommand;