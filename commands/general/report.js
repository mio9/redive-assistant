const { Command } = require("discord-akairo");
const command = require("../../lib/command-info.json");
const strings = require("../../lib/strings.json");
const UtilLib = require("../../api/util-lib");
const { ProgressManager, SettingsManager } = require("../../api/storage-lib");
const Log = require("../../classes/LogContent");
const parse = require("../../api/parse-progress");;

class ReportCommand extends Command {
   constructor() {
      super("report", {
         aliases: command.commandList.report.alias,
         cooldown: 5000,
         channel: "guild",
         args: [
            {
               id: "reportValue",
               type: "string",
            },
            {
               id: "matchType",
               type: "character",
               default: "h"
            }
         ]
      });
   }

   async userPermissions(member) {
      return SettingsManager.checkMember(member);
   }

   async exec(message, args) {

      let loadingMsg = await message.channel.send(UtilLib.randomLoading());
      if (!args.reportValue) return loadingMsg.edit(strings.error.missing_input);

      //replace excess words, then parse integer
      let reportValue = parseInt(args.reportValue.replace(/(w|W|萬)/g, ""));
      //return an error if the input is invalid
      if (isNaN(reportValue)) return loadingMsg.edit(strings.error.invalid_input_number);

      const type = args.matchType;
      let returnHP;
      const maxHP = await ProgressManager.getCurrentMaxHP(message.guild.id);
      const currentHP = await this.client.serverInfo.get(message.guild.id, "current_boss_HP");

      if (reportValue > 3000) { // entered full damage
         reportValue = Math.round(reportValue / 10000);
      };

      if (type == "h") { // HP left
         returnHP = Math.max(reportValue, 0);
      } else if (type == "d" || type == "-") { // damage done
         returnHP = currentHP - reportValue;
      };

      //check if NOT in range
      if (type == "h" && // report hp directly
         ((reportValue > maxHP) || (reportValue < 0))) { // the return hp is larger than 100% or below 0
         return loadingMsg.edit(strings.error.invalid_input_number);
      }

      //trigger broadcast before activating .next
      const avatarURL = message.author.displayAvatarURL();
      const member = await message.guild.members.fetch(message.author.id)
      const username = UtilLib.extractInGameName(member.displayName, true)
      const content = {
         username: username,
         hpLeft: Math.max(returnHP, 0)
      }
      const log = new Log("report", content, "embed", avatarURL);
      this.client.emit("broadcast", message.guild, log.output, "log");


      //triggers .next if below 0
      if (returnHP <= 0) {

         const nextReturn = await ProgressManager.nextBoss(message.guild.id, message);
         //return extra message
         const outputString = strings.report.report_success.replace("%hpLeft", 0)
            + "\n" + strings.report.report_below_zero.replace("%progress", parse.textUnparseAll(nextReturn.progressNum));
         loadingMsg.edit(outputString);

         const content = {
            progressNum: nextReturn.progressNum
         };

         const log = new Log("next", content, "embed");
         this.client.emit("broadcast", message.guild, log.output, "log");

      } else {
         //update hp data to database
         let successReturnString = strings.report.report_success.replace("%hpLeft", returnHP)
         if (returnHP > currentHP) {
            successReturnString += `\n${strings.report.report_excess_value}` // add error if reported value is greater than current
         };

         loadingMsg.edit(successReturnString);
         await this.client.serverInfo.update(message.guild.id, "current_boss_HP", returnHP);
      };
      this.client.emit("knifeUpdate", message.guild);
      return;
   };

};

module.exports = ReportCommand;