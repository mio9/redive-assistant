const { Command } = require("discord-akairo");
const strings = require("../../lib/strings.json");
const UtilLib = require("../../api/util-lib");
const { MessageEmbed } = require('discord.js');
const command = require("../../lib/command-info.json");
const { KnifeReportManager, UserManager, SettingsManager } = require("../../api/storage-lib");
const parse = require("../../api/parse-progress");

class ListCommand extends Command {
   constructor() {
      super("list", {
         aliases: command.commandList.list.alias,
         args: [{
            id: "user",
            type: "member"
         }]
      });
   }

   async userPermissions(member) {
      return SettingsManager.checkMember(member);
   }

   async exec(message, args) {

      // set loading message
      let loadingMsg = await message.channel.send(`${UtilLib.randomLoading()}`);

      // select command user if missing argument
      const user = !args.user ? message.author : args.user.user;

      //create embed
      const member = await message.guild.members.fetch(user.id);
      const username = UtilLib.extractInGameName(member.displayName, true);
      const avatarURL = user.displayAvatarURL();
      const compStatus = await UserManager.compensateCheck(message.guild.id, user.id)
      const userTitle = compStatus ? `🔸 ${username}` : `🔹 ${username}`;

      const embed = new MessageEmbed()
         .setAuthor(userTitle, avatarURL)
         .setColor("#f0f0f0");

      // create data list for checking the database
      const reportQuery = {
         user_id: user.id
      };

      // get data from database
      const reportQueryArr = await KnifeReportManager.getReports(message.guild.id, reportQuery, "asc");

      // console.log(reportQueryArr);

      if (reportQueryArr.length == 0) {
         // no record
         embed.setDescription(strings.knifeReport.no_record);
      } else {
         for (let i in reportQueryArr) {

            //pulling out data

            let reportData = reportQueryArr[i].data;

            const progress = parse.textUnparseAll(reportData.progress_num);

            const type = reportData.type;
            const comment = reportData.comment;

            const date = new Date(reportData.time);
            const time = UtilLib.formatDate(date);

            let knifeTitle = reportData.is_compensate ? `🔸 ${progress} | ${type}` : `🔹 ${progress} | ${type}`;
            if (reportData.is_completed == true) { // handle complete
               knifeTitle += " ✅";
            };
            const knifeDetail = comment ? `備註：${comment}\n時間：${time}` : `時間：${time}`

            embed.addField(knifeTitle, knifeDetail)
         }
      };

      return loadingMsg.delete().then(
         message.channel.send(embed)
      );
   };
};

module.exports = ListCommand;