const { Command } = require("discord-akairo");
const { SettingsManager } = require("../../api/storage-lib")

class StartBattleCommand extends Command {
   constructor() {
      super("startbattle", {
         aliases: ["start", "st"],
      });
   };

   async userPermissions(member) {
      return SettingsManager.checkMember(member);
   }

   async exec(message) {

      message.channel.send("❕ 此指令已被 `.finish` 取代，有需要請查閱指令教學。\n")

   };
};

module.exports = StartBattleCommand;