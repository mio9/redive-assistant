const { Command } = require("discord-akairo");
const strings = require("../../lib/strings.json");
const { MessageEmbed } = require('discord.js');
const command = require("../../lib/command-info.json");
const UtilLib = require("../../api/util-lib");
const { KnifeReportManager, InfoManager, SettingsManager } = require("../../api/storage-lib");
const parse = require("../../api/parse-progress");


class BossListCommand extends Command {
   constructor() {
      super("bosslist", {
         aliases: command.commandList.bosslist.alias,
         args: [
            {
               id: "week",
               type: "number",
               prompt: {
                  start: strings.prompt.week,
                  retry: strings.prompt.not_a_number,
               },
            },
            {
               id: "boss",
               type: "number",
               prompt: {
                  start: strings.prompt.boss,
                  retry: strings.prompt.not_a_number,
               },
            },
         ]
      });
   };

   async userPermissions(member) {
      return SettingsManager.checkMember(member);
   }

   async exec(message, args) {
      let loadingMsg = await message.channel.send(UtilLib.randomLoading());

      //database access
      const reportQuery = {
         progress_num: parse.parse(args.week, args.boss)
      };

      const reportQueryArr = await KnifeReportManager.getReports(message.guild.id, reportQuery, "asc");

      //generating embed
      const embed = new MessageEmbed()
         .setColor("#90ffff")
         .setTitle(`${args.week} 周 ${args.boss} 王`);

      //get current boss data
      const bossData = await InfoManager.get(message.guild.id, "boss_data");
      const bossName = bossData.boss_name[args.boss - 1];

      const currentStage = UtilLib.convertWeekToStage(args.week);
      const bossMaxHP = bossData.boss_max_hp[currentStage][args.boss - 1];


      embed.setDescription(`名稱： ${bossName}\n最大血量： ${bossMaxHP}萬`);

      if (reportQueryArr.length == 0) {
         embed.addField(strings.knifeReport.no_record, "在空白的報刀表當中尋找記錄的人腦袋一定有問題")
      }
      else {
         for (let i in reportQueryArr) {

            let reportData = reportQueryArr[i].data;

            const type = reportData.type;
            const comment = reportData.comment;
            const date = new Date(reportData.time)
            const time = UtilLib.formatDate(date);
            const username = reportData.username;

            let thisReportTitle = reportData.is_compensate ? `${username} | 🔸 ${type}` : `${username} | 🔹 ${type}`;
            if (reportData.is_completed == true) {
               thisReportTitle += " ✅";
            };
            const thisReportDetail = comment ? `備註：${comment}\n時間：${time}` : `時間：${time}`;

            embed.addField(thisReportTitle, thisReportDetail);
         };
      };

      //JSON.stringify(arr)
      return loadingMsg.delete().then(
         message.channel.send(embed)
      );

   };

};

module.exports = BossListCommand;