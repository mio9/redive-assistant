const { Command } = require("discord-akairo");
const strings = require("../../lib/strings.json");
const OpenCC = require('opencc');
const converter = new OpenCC('s2t.json');
const command = require("../../lib/command-info.json");
const ReportContentHandler = require("../../api/ReportContentHandler");
const UtilLib = require("../../api/util-lib")
const { KnifeReportManager, SettingsManager } = require("../../api/storage-lib");
const ReportObject = require("../../classes/ReportObject");
const Log = require("../../classes/LogContent");

class KnifeCommand extends Command {
   constructor() {
      super("knife", {
         aliases: command.commandList.knife.alias,
         cooldown: 3000,
         args: [

            {
               id: "compensate",
               match: "flag",
               flag: command.flags.compensate
            },
            {
               id: "week",
               type: "integer",
               prompt: {
                  start: strings.prompt.week,
                  retry: strings.not_a_number,
               },
            },
            {
               id: "boss",
               type: "integer",
               prompt: {
                  start: strings.prompt.boss,
                  retry: strings.not_a_number,
               },
            },
            {
               id: "detail",
               type: "string",
               match: "restContent"
            }
         ],
      });
   };

   async userPermissions(member) {
      return SettingsManager.checkMember(member);
   }

   async exec(message, args) {

      const report = new ReportObject({});

      let loadingMsg = await message.channel.send(UtilLib.randomLoading());
      let statusString;

      const week = args.week;
      const boss = args.boss;

      // check if the boss number is in range, prompt error if not
      if (boss < 1 || boss > 5) {
         return loadingMsg.edit(strings.knifeReport.boss_out_of_range.replace("%entry", `${boss}`));
      };

      let detail;
      if (args.detail) {
         detail = await converter.convertPromise(args.detail) + " "; //adding a space for regex to replace flag only
         detail = detail.replace(/(-|)(c|C)+ /g, "")
      } else {
         detail = "";
      };

      const isCompensate = args.compensate;

      const member = await message.guild.members.fetch(message.author.id)

      report.setAll(member, week, boss, detail, isCompensate);

      const resultObject = await KnifeReportManager.add(message.guild.id, report);
      if (resultObject.status == true) {
         statusString = strings.knifeReport.add_success;
      }
      else {
         // return the error
         return loadingMsg.edit(resultObject.data);
      };

      this.client.emit("knifeUpdate", message.guild);

      const content = {
         username: report.username,
         progressNum: report.progress_num,
         type: report.type,
         isCompensate: report.is_compensate
      };


      const avatarURL = message.author.displayAvatarURL();
      const log = new Log("knifeReportAdd", content, "embed", avatarURL);
      this.client.emit("broadcast", message.guild, log.output, "log");

      const embed = ReportContentHandler.composeEmbed(report, "default", report.error, avatarURL);
      // knife report complete, print out object
      loadingMsg.edit(statusString);

      if (statusString == strings.knifeReport.add_success) { loadingMsg.edit(embed) };
      return;

   };

}
module.exports = KnifeCommand;

