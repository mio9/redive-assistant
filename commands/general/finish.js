/* eslint-disable no-case-declarations */
const { Command } = require("discord-akairo");
const ReportContentHandler = require("../../api/ReportContentHandler");
const { ProgressManager, KnifeReportManager, UserManager, SettingsManager } = require("../../api/storage-lib");
const ReportObject = require("../../classes/ReportObject");
const UtilLib = require("../../api/util-lib");
const LogContent = require("../../classes/LogContent");
const command = require("../../lib/command-info.json");
const parse = require("../../api/parse-progress");
const strings = require("../../lib/strings.json");
const { MessageEmbed } = require("discord.js");

class FinishCommand extends Command {
   constructor() {
      super("finish", {
         aliases: command.commandList.finish.alias,
         cooldown: 10000,
         channel: "guild",
         args: [
            {
               id: "next",
               match: "flag",
               flag: command.flags.next
            }
         ]
      });
   };

   async userPermissions(member) {
      return SettingsManager.checkMember(member);
   }

   async exec(message, args) {

      const loadingString = UtilLib.randomLoading();           // used for editing the loading message multiple times
      let loadingMsg = await message.channel.send(loadingString);
      const guildID = message.guild.id;

      const progress = await ProgressManager.getProgress(guildID);
      const user = await message.guild.members.fetch(message.author.id);
      const avatarURL = message.author.displayAvatarURL();

      // const serverDocRef = await this.client.db
      //    .collection("servers")
      //    .doc(message.guild.id)
      // const serverDoc = serverDocRef.get();

      const normalQuery = {
         progress_num: progress,
         user_id: message.author.id,
         is_compensate: false,      // NOT compensate
         is_completed: false
      };
      const compensateQuery = {
         progress_num: progress,
         user_id: message.author.id,
         is_compensate: true,       // compensate
         is_completed: false
      };

      const getreports2 = KnifeReportManager.getReports(guildID, normalQuery, "asc");
      const getreports3 = KnifeReportManager.getReports(guildID, compensateQuery, "asc");
      const getcompstatus = UserManager.compensateCheck(guildID, message.author.id);

      const [normReports, compReports, compStatus] = await Promise.all([getreports2, getreports3, getcompstatus])


      let finishString; // used for editing the loading message multiple times
      let thisReport;   // final output of edited report
      let autoReport; // temp variable name

      // ternary number gen
      /*
      3^0 Normal Reports      | No reports  | One report | More than one reports
      3^1 Compensate Reports  | No reports  | One report | More than one reports
      3^2 Compensate Status   | Hold Normal | Hold Comp  | Null  
      */
      let normSwitch, compSwitch, compStatSwitch;
      switch (normReports.length) { // 3^0: normal
         case 0:                    // no reports
            normSwitch = 0;
            break;
         case 1:                    // 1 report
            normSwitch = 1;
            break;
         default:                   // 2+ reports
            normSwitch = 2;
            break;
      };
      switch (compReports.length) { // 3^1: compensate
         case 0:                    // no reports
            compSwitch = 0
            break;
         case 1:                    // 1 report
            compSwitch = 1;
            break;
         default:                   // 2+ reports
            compSwitch = 2;
            break;
      };
      switch (compStatus) {
         case false:
            compStatSwitch = 0;
            break;
         case true:
            compStatSwitch = 1;
            break;
      };

      const ternarySwitch = normSwitch + 3 * compSwitch + 9 * compStatSwitch;

      /*
      Normal (N)          Compensate (C)   (Status)
      0  00       [C]      9  00       [C]
      1  10       [M]     10  10       [A]
      2  N0 (NL)  [M]     11  N0 (NL)  [A]
      3  01       [A]     12  01       [M]
      4  11       [M]     13  11       [M]
      5  N1 (NL)  [M]     14  N1       [M]
      6  0N (CL)  [A]     15  0N (CL)  [M]
      7  1N       [M]     16  1N (CL)  [M]
      8  NN (NL)  [M]     17  NN (CL)  [M]

      00: no normal, no compensate | 1N: one normal, more than one compensate

      (NL)/(CL): requires selection from normal / compensate report list
      [C]: create new report of corresponding type
      [M]: match a report of corresponding type
      [A]: ask for change from opposite type

      let functions:
      A(c) = Auto create reports for corresponding status c
      M(c) = Match corresponding status report and mark as complete
      L(c) = List all reports for corresponding status c and allow user to choose one
      E(c) = Edit a selected report to the OPPOSITE type of status
  
         0         A(N)                    Type 1N PASS
         1         M(N)                  Type 2aN  PASS  
         2  L(N) > M(N)                Type 2bN    PASS
         3  L(C) > A(N) || E(C)     Type 3N        PASS PASS
         4         M(N)                  Type 2aN  PASS
         5  L(N) > M(N)                Type 2bN    PASS
         6  L(C) > A(N) || E(C)     Type 3N        PASS PASS
         7         M(N)                  Type 2aN  PASS
         8  L(N) > M(N)                Type 2bN    PASS
         9         A(C)                    Type 1C PASS
        10  L(N) > A(C) || E(N)     Type 3C        PASS PASS
        11  L(N) > A(C) || E(N)     Type 3C        PASS PASS
        12         M(C)                  Type 2aC  PASS
        13         M(C)                  Type 2aC  PASS                 
        14         M(C)                  Type 2aC  PASS
        15  L(C) > M(C)                Type 2bC    PASS
        16  L(C) > M(C)                Type 2bC    PASS   
        17  L(C) > M(C)                Type 2bC    PASS

      Type 1: Auto(c)
      Type 2a: Match(c)
      Type 2b: List(c) > Match (c)
      Type 3: List(!c) > ?Edit ? Edit(!c) : Auto(c) 

      FINAL

      Type 1:  0,9
      Type 2a: 1,4,7,12,13,14
      Type 2b: 2,5,8,15,16,17
      Type 3:  3,6,10,11
      */

      let selectionReports, selectionReturn, listEmbed, selectionString;
      switch (ternarySwitch) {
         case 0:
         case 9:

            // Type 1: Submit AutoReport of current compensate status

            finishString = strings.finish.auto_report;
            loadingMsg.edit(finishString + `\n` + loadingString);

            // create auto report with current compensate status
            autoReport = await this._submitAutoReport(guildID, user, progress, compStatus);

            if (typeof (autoReport.status) == "string") {  // returned an error from KRM.add()
               return loadingMsg.edit(strings.finish.auto_report_fail + `\n` + autoReport.status);
            };

            thisReport = autoReport.report;

            break;

         case 1:
         case 4:
         case 7:
         case 12:
         case 13:
         case 14:

            // Type 2a: Auto match the corresponding report and then mark as complete

            finishString = strings.finish.report_found.replace("%count", "1");
            loadingMsg.edit(finishString + `\n` + loadingString);

            // mark knife as complete
            if (compStatus == false) {
               // normal knife
               thisReport = await this._markComplete(guildID, normReports[0].id);
            } else {
               // compensate
               thisReport = await this._markComplete(guildID, compReports[0].id);
            };

            break;

         case 2:
         case 5:
         case 8:
         case 15:
         case 16:
         case 17:

            // Type 2b: Select an report from generated list, and then mark as complete

            //list of reports to be used, depends on compensate status
            selectionReports = compStatus ? compReports : normReports;

            // create embed list
            listEmbed = this._createReportList(selectionReports, progress, avatarURL);
            loadingMsg.edit(strings.finish.report_found.replace("%count", `${selectionReports.length}`));
            loadingMsg.edit(listEmbed);

            // ask for selection
            selectionReturn = await UtilLib.emojiResponseSelection(message.author.id, loadingMsg, selectionReports.length);
            if (selectionReturn == false) {  // handles selection timeout
               return;
            };

            // mark corresponding report
            thisReport = await this._markComplete(guildID, selectionReports[selectionReturn - 1].id);

            break;

         case 3:
         case 6:
         case 10:
         case 11:

            //  Type 3: Generate a list of opposite compensate reports, and then 
            //          choose to edit to correct status, or create autoreport

            selectionReports = compStatus ? normReports : compReports;  // note that this is in reverse

            listEmbed = this._createReportList(selectionReports, progress, avatarURL);

            const numberArr = ["1️⃣", "2️⃣", "3️⃣", "4️⃣", "5️⃣", "6️⃣"];
            // set finish string
            selectionString = compStatus ? strings.finish.normal_report_only : strings.finish.compensate_report_only;
            if (selectionReports.length == 1) {
               selectionString = selectionString.replace("%count", "1️⃣");
            } else {
               selectionString = selectionString.replace("%count", `1️⃣ - ${numberArr[selectionReports.length - 1]}`)
            }


            loadingMsg.edit(selectionString); loadingMsg.edit(listEmbed);

            // generate selection emote array

            let selectionEmoteArr = [];

            for (let i = 0; i < selectionReports.length; i++) {
               selectionEmoteArr.push(numberArr[i]);
            };
            selectionEmoteArr.push("🔄");

            // ask for selection
            selectionReturn = await UtilLib.emojiResponseSelection(message.author.id, loadingMsg, selectionReports.length + 1, selectionEmoteArr);
            if (selectionReturn == false) {  // handles selection timeout
               return;
            };

            // 1 ~ length: selected array | length + 1: auto-gen
            if (selectionReturn == selectionReports.length + 1) {
               // auto report
               autoReport = await this._submitAutoReport(guildID, user, progress, compStatus);
               if (typeof (autoReport.status) == "string") {  // returned an error from KRM.add()
                  return loadingMsg.edit(strings.finish.auto_report_fail + `\n` + autoReport.status);
               };
               thisReport = autoReport.report;
            }
            else {
               // edit report
               const editData = {
                  is_completed: true,
                  is_compensate: compStatus,
               };
               thisReport = await KnifeReportManager.edit(guildID, selectionReports[selectionReturn - 1].id, editData);
            };

            break;

         default:
      };



      // generate end message

      const content = {
         username: thisReport.username,
         progressNum: progress,
         isCompensate: thisReport.is_compensate,
         type: thisReport.type,
         comment: thisReport.comment

      };

      const embed = ReportContentHandler.composeEmbed(thisReport, "#efffff", thisReport.error, avatarURL);
      loadingMsg.delete();
      message.channel.send(embed);
      const log = new LogContent("finish", content, "embed", avatarURL);
      this.client.emit("broadcast", message.guild, log.output, "log");

      // handles the "next" flag
      let compEditType;
      if (args.next == true) {

         // edit type = next
         compEditType = "next";

         // do PM.nextBoss()
         let nextMsg = await message.channel.send(loadingString);
         const nextReturn = await ProgressManager.nextBoss(message.guild.id, message);
         const nextLogContent = {
            progressNum: nextReturn.progressNum
         };

         // logging
         const log = new LogContent("next", nextLogContent, "embed");
         this.client.emit("broadcast", message.guild, log.output, "log");
         nextMsg.edit(strings.next.success.replace("%progress", parse.textUnparseAll(nextReturn.progressNum)));

      } else {
         compEditType = "finish"
      };

      await UserManager.compensateEdit(guildID, message.author.id, compEditType);

      this.client.emit("knifeUpdate", message.guild);

      return;

   };

   async _submitAutoReport(guildID, member, progress, isCompensate = false) {
      const parseResult = parse.unparseAll(progress);

      const report = new ReportObject({});
      report.setAll(member, parseResult.week, parseResult.boss, "", isCompensate)
      report.setError(strings.finish.auto_report_footer);
      report.setComplete();
      report.type = strings.finish.auto_report_detail;

      const returnObj = await KnifeReportManager.add(guildID, report);

      let result = {
         status: null,
         report: report
      }
      if (returnObj.status == false) { // some error
         result.status = returnObj.data;
      } else {
         result.status = true;
      };

      return result;
   };

   async _markComplete(guildID, reportID) {
      return await KnifeReportManager.edit(guildID, reportID, { is_completed: true });
   };

   _createReportList(reportsArr, currentProgress, avatarURL) {

      // create a list of reports
      const listEmbed = new MessageEmbed()
         .setColor("#EFFFFF")
         .setAuthor(`${reportsArr[0].data.username} | ${parse.textUnparseAll(currentProgress)}`, avatarURL);
      // loop data
      for (let i in reportsArr) {
         let reportData = reportsArr[i].data;

         const type = reportData.type;
         const comment = reportData.comment;

         const date = new Date(reportData.time);
         const time = UtilLib.formatDate(date);

         const thisReportTitle = reportData.is_compensate ? `${parseInt(i) + 1} | 🔸 ${type}` : `${parseInt(i) + 1} | 🔹 ${type}`;
         const thisReportDetail = comment ? `備註：${comment}\n時間：${time}` : `時間：${time}`;

         listEmbed.addField(thisReportTitle, thisReportDetail);
      };

      return listEmbed;
   }

}
module.exports = FinishCommand