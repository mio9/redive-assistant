const { Command } = require("discord-akairo");
const strings = require("../../lib/strings.json");
const command = require("../../lib/command-info.json");
const UtilLib = require("../../api/util-lib");
const { ProgressManager, SettingsManager, UserManager } = require("../../api/storage-lib");
const Log = require("../../classes/LogContent");
const parse = require("../../api/parse-progress");


class NextCommand extends Command {

   constructor() {
      super("next", {
         aliases: ["n", "next"],
         // aliases: command.commandList.next.alias,
         cooldown: 20000,
         channel: "guild",
         args: [
            {
               id: "user",
               type: "member"
            },
            {
               id: "pseudoflag",
               match: "flag",
               flag: ["-f", "-force"]
            },
            // {
            //    id: "editCompensateTrue",
            //    match: "flag",
            //    flag: ["true"]
            // },
            // {
            //    id: "editCompensateFalse",
            //    match: "flag",
            //    flag: ["false"]
            // }
         ]
      });
   };

   async userPermissions(member) {
      return SettingsManager.checkMember(member);
   }

   async exec(message, args) {

      const loadingString = UtilLib.randomLoading();
      let loadingMsg = await message.channel.send(loadingString);

      const deprecatedMsg =
         "❕ 原有 `.next` 功能已移至 `.finish`，有需要請查閱指令教學。\n"
         + "如果你已完成出刀但未使用`.f`，請使用 `.f next`。\n"
         + "如果你已經使用了`.f`，請使用 `.undo` 取消該出刀記錄，並重新使用 `.f next`。";

      let user = message.author;

      if (!args.user && args.pseudoflag == true) {  // no user, editing self
         true;                                      // do nothing

      } else if (!args.user) {   // no user but no force tag
         return loadingMsg.edit(deprecatedMsg); // return deprecated message

      } else if (args.user) {    // tagging other user
         user = args.user;                // set user from arg

      } else {                   // other impossible scenario
         return loadingMsg.edit(strings.error.error);
      }



      const member = await message.guild.members.fetch(user.id);
      const username = UtilLib.extractInGameName(member.displayName, false);
      const editCompSelectionString = strings.next.edit_compensate_prompt.replace("%user", username);
      loadingMsg.edit(editCompSelectionString);

      const response = await UtilLib.emojiResponseSelection(message.author.id, loadingMsg, 2, ["✅", "❌"]);

      let editcomp;

      if (response == 1) {
         editcomp = true;
      } else if (response == 2) {
         editcomp = false;
      } else return;

      // }

      loadingMsg.edit(loadingString);

      if (editcomp == true) {
         await UserManager.compensateRevert(message.guild.id, user.id);
         await UserManager.compensateEdit(message.guild.id, user.id, "next");
      }

      const nextReturn = await ProgressManager.nextBoss(message.guild.id, message);

      this.client.emit("knifeUpdate", message.guild);

      const logContent = {
         progressNum: nextReturn.progressNum
      };

      const log = new Log("next", logContent, "embed");
      this.client.emit("broadcast", message.guild, log.output, "log");

      return loadingMsg.edit(strings.next.success.replace("%progress", parse.textUnparseAll(nextReturn.progressNum)));
   };
};

module.exports = NextCommand;
