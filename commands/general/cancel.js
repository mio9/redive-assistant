const { Command } = require("discord-akairo");
const strings = require("../../lib/strings.json");
const command = require("../../lib/command-info.json");
const { KnifeReportManager, SettingsManager } = require("../../api/storage-lib");
const UtilLib = require("../../api/util-lib");
const ReportContentHandler = require("../../api/ReportContentHandler");
const { MessageEmbed } = require("discord.js");
const Log = require("../../classes/LogContent");
const parse = require("../../api/parse-progress");

class CancelCommand extends Command {
   constructor() {
      super("cancel", {
         aliases: command.commandList.cancel.alias,
         args: [
            {
               id: "all",
               match: "flag",
               flag: command.flags.selectAll
            },
            {
               id: "week",
               type: "integer",
               prompt: {
                  optional: true,
                  start: strings.prompt.week,
                  retry: strings.prompt.not_a_number,
               },
            },
            {
               id: "boss",
               type: "integer",
               prompt: {
                  optional: true,
                  start: strings.prompt.boss,
                  retry: strings.prompt.not_a_number,
               },
            },
         ]
      });
   };

   async userPermissions(member) {
      return SettingsManager.checkMember(member);
   }

   async exec(message, args) {

      let loadingMsg = await message.channel.send(UtilLib.randomLoading());
      const deleteProgressNum = parse.parse(args.week, args.boss);

      let reportQuery = {
         user_id: `${message.author.id}`,
         is_completed: false
      };

      if (args.week && args.boss) {
         reportQuery.progress_num = deleteProgressNum;
      };

      const reportArr = await KnifeReportManager.getReports(message.guild.id, reportQuery);

      // zero length: return no record
      if (reportArr.length == 0) {
         return loadingMsg.edit(strings.knifeReport.no_record);
      };

      // always get first report for reference
      const deletedReport = reportArr[0].data;
      // message.channel.send(`\`\`\`${JSON.stringify(deletedReport)}\`\`\``);

      // user avatar
      const avatarURL = message.author.displayAvatarURL();

      let embed, statusString;
      if (args.all == false) {            // delete only first report in array
         await KnifeReportManager.delete(message.guild.id, [reportArr[0]]);
         embed = ReportContentHandler.composeEmbed(deletedReport, "#e60000", null, avatarURL);
         statusString = strings.knifeReport.cancel_success;
      }

      else if (args.all == true) {        // delete all report in array
         await KnifeReportManager.delete(message.guild.id, reportArr);
         statusString = strings.knifeReport.cancel_all_success;
         //compose feedback embed
         embed = new MessageEmbed()
            .setAuthor(`${deletedReport.username} | ${reportArr.length}筆記錄`, message.author.displayAvatarURL());

         if (args.week && args.boss) {
            embed.setDescription(`指定目標: ${args.week}周${args.boss}王`);
         } else {
            embed.setDescription(`無指定周目 (所有報刀)`);
         };

      } else {
         return loadingMsg.edit(strings.error.error);
      };

      embed.setFooter(strings.knifeReport.delete_embed_footer);
      loadingMsg.edit(embed);
      loadingMsg.edit(statusString);

      this.client.emit("knifeUpdate", message.guild);

      const member = await message.guild.members.fetch(message.author.id)
      const username = UtilLib.extractInGameName(member.displayName, true);

      let content = {
         username: username,
         isCompensate: deletedReport.is_compensate
      };
      let cancelType;

      if (args.all == true) { // cancel ALL

         if (args.week && args.boss) content.progressNum = parse.parse(args.week, args.boss);
         content.count = reportArr.length;
         cancelType = "knifeReportCancelAll"

      } else { // cancel 1

         content.progressNum = deletedReport.progress_num;
         content.type = deletedReport.type;
         cancelType = "knifeReportCancel"

      };

      const log = new Log(cancelType, content, "embed", avatarURL);
      this.client.emit("broadcast", message.guild, log.output, "log")

      return;
   };

};

module.exports = CancelCommand;