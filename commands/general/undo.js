const { Command } = require("discord-akairo");
const { UserManager, KnifeReportManager, SettingsManager } = require("../../api/storage-lib");
const { MessageEmbed } = require("discord.js");
const UtilLib = require("../../api/util-lib");
const strings = require("../../lib/strings.json");
const command = require("../../lib/command-info.json");
const ReportContentHandler = require("../../api/ReportContentHandler");
const Log = require("../../classes/LogContent");

class UndoCommand extends Command {
   constructor() {
      super("undo", {
         aliases: command.commandList.undo.alias,
      });
   };

   async userPermissions(member) {
      return SettingsManager.checkMember(member);
   }

   async exec(message) {

      let loadingMsg = await message.channel.send(UtilLib.randomLoading());
      const avatarURL = message.author.displayAvatarURL()

      // query completed reports
      let query = {
         user_id: message.author.id,
         is_completed: true
      };

      const reportsArr = await KnifeReportManager.getReports(message.guild.id, query, "desc");

      if (reportsArr.length == 0) { // no reports found

         return loadingMsg.edit(strings.knifeReport.no_record);

      };

      // revert compensate status
      const revertReturn = await UserManager.compensateRevert(message.guild.id, message.author.id);

      if (typeof revertReturn == "string") { // prev record is null
         return loadingMsg.edit(revertReturn)
      };


      // revert report
      const editedReport = await KnifeReportManager.edit(message.guild.id, reportsArr[0].id, { is_completed: false });
      const returnEmbed = ReportContentHandler.composeEmbed(editedReport, "#60e0f0", null, avatarURL);

      loadingMsg.edit(strings.undo.success); loadingMsg.edit(returnEmbed);
      this.client.emit("knifeUpdate", message.guild);



      const content = {
         progressNum: editedReport.progress_num,
         isCompensate: editedReport.is_compensate,
         type: editedReport.type,
         username: editedReport.username
      }

      const log = new Log("undo", content, "embed", avatarURL);
      this.client.emit("broadcast", message.guild, log.output, "log");

      return;
   }

}

module.exports = UndoCommand