const { Command } = require("discord-akairo");

class PingCommand extends Command {
   constructor() {
      super("ping", {
         aliases: ["ping", "🏓"],
      });
   };

   async exec(message) {

      // const serverInfo = await this.client.serverInfo.get(message.guild.id)
      // message.channel.send(JSON.stringify(serverInfo))

      let initialTime = message.createdTimestamp;
      let sentmsg = await message.channel.send('等候中...');
      let arrivingTime = sentmsg.createdAt;

      let timeDiff = arrivingTime - initialTime;
      sentmsg.edit(`🏓 PONG! 延遲值：${timeDiff}ms `);
   }
}
module.exports = PingCommand;