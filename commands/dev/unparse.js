const parse = require("../../api/parse-progress");
const { Command } = require("discord-akairo");
const { Permissions } = require("discord.js");

class UnparseCommand extends Command {
   constructor() {
      super("unparse", {
         aliases: ["unparse", "up"],
         userPermissions: Permissions.FLAGS.ADMINISTRATOR,
         args: [
            {
               id: "progressID",
               type: "number"
            }
         ]
      });
   }

   async exec(message, args) {
      return message.channel.send(`\`\`\`${parse.unparse(args.progressID, "week")}周 ${parse.unparse(args.progressID, "boss")}王\`\`\``)
   };

};

module.exports = UnparseCommand;