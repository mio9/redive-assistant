const { Command } = require("discord-akairo");

class TestCommand extends Command {
  constructor() {
    super("adr-test", {
      aliases: ["adr"],
      ownerOnly: true,
      channel: "guild",
    });
  }

  async exec(message) {
    this.client.emit("dailyReset")
  }
}
module.exports = TestCommand;
