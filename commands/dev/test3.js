const { Command } = require("discord-akairo");
const { Permissions } = require("discord.js");

class Test3Command extends Command {
  constructor() {
    super("test3", {
      aliases: ["test3"],
      userPermissions: Permissions.FLAGS.ADMINISTRATOR,
      ownerOnly: true,
      args: [
      ],
    });
  };

  async exec(message, args) {

    let doc = await this.client.cachedb.findOne({ _id: message.guild.id });
    return message.channel.send(doc);

  };
};

module.exports = Test3Command;