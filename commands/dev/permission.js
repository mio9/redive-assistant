/* eslint-disable no-case-declarations */
const { Command } = require("discord-akairo");
const { SettingsManager } = require("../../api/storage-lib");

class PermissionCommand extends Command {
   constructor() {
      super("permission", {
         aliases: ["perm", "permission"],
         args: [
         ],
      });
   };

   async exec(message) {
      const isSysAdmin = !message.member.hasPermission('ADMINISTRATOR')
      const isAdmin = await SettingsManager.checkAdmin(message);
      const isMember = await SettingsManager.checkMember(message);

      if (!isSysAdmin) {
         return message.channel.send("`系統管理員 (層級3)`")
      } else if (!isAdmin) {
         return message.channel.send("`管理員 (層級2)`")
      } else if (!isMember) {
         return message.channel.send("`成員 (層級1)`")
      } else {
         return message.channel.send("`無 (層級0)`")
      }
   }
};

module.exports = PermissionCommand;