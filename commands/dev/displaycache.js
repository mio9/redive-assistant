const { Command } = require("discord-akairo");

class DisplayCacheCommand extends Command {
  constructor() {
    super("display-cache", {
      aliases: ["dica"],
      ownerOnly: true,
      channel: "guild",
    });
  }

  async exec(message) {
    let docs = await this.client.cachedb.find({});
    message.channel.send("dica cmd executed");
    return console.log(docs);
  }
}
module.exports = DisplayCacheCommand;
