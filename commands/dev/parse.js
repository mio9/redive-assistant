const parse = require("../../api/parse-progress");
const { Command } = require("discord-akairo");
const { Permissions } = require("discord.js");

class ParseCommand extends Command {
   constructor() {
      super("parse", {
         aliases: ["parse", "p"],
         userPermissions: Permissions.FLAGS.ADMINISTRATOR,
         args: [
            {
               id: "week",
               type: "number",
            },
            {
               id: "boss",
               type: "number",
               default: 1
            }
         ]
      });
   }

   async exec(message, args) {
      return message.channel.send(`\`\`\`ID: ${parse.parse(args.week, args.boss)}\`\`\``);
   };

};

module.exports = ParseCommand;