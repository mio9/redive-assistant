const { Command } = require("discord-akairo");
const strings = require("../../lib/strings.json");

class ZuorhiViyantasWasFestsuRuorProi extends Command {
   constructor() {
      super("conflict", {
         aliases: ["conflict"],
      });
   }



   async exec(message) {

      const conflict = strings.conflict;
      const beatms = 375;

      // In a desperate conflict 

      message.channel.send(conflict[0]);
      await sleep(7 * beatms); // 7 beats

      // with a ruthless enemy... 
      message.channel.send(conflict[1]);
      await sleep(6 * beatms); // 6 beats

      // zuorhi viyantas was festsu ruor proi
      message.channel.send(conflict[2]);
      await sleep(8.5 * beatms); // 8 beats

      // yuk dalfe suoivo swenne yat vu henvi nes
      message.channel.send(conflict[3]);
      await sleep(8.5 * beatms); // 8 beats

      // sho fu briyu praffi stassui tsenva chies
      message.channel.send(conflict[4]);
      await sleep(8 * beatms); // 8 beats

      // ien ryus sois nyat pyaro shennie fru
      message.channel.send(conflict[5]);
      await sleep(8 * beatms) // 7.5 beats

      // prasueno turoden shes vi hyu vu praviya 
      message.channel.send(conflict[6]);
      await sleep(8 * beatms) // 8 beats

      // tyu prostes fis hien hesnie ryanmie proshuka 8
      message.channel.send(conflict[7]);
      await sleep(8 * beatms) // 8 beats

      // wi swen ryasta grouts froine shienhie var yat 9
      message.channel.send(conflict[8]);
      await sleep(9 * beatms) // 9 beats

      // nyam raika rit skuois trapa tof
      message.channel.send(conflict[9]);

      return;

      function sleep(ms) {
         return new Promise(resolve => setTimeout(resolve, ms));
      }


   }
}
module.exports = ZuorhiViyantasWasFestsuRuorProi;