# ⚔ Re:Dive Assistant / Re:Dive 戰隊戰助手

[![pipeline status](https://gitlab.com/mio9/redive-assistant/badges/master/pipeline.svg)](https://gitlab.com/mio9/redive-assistant/-/commits/master)

## ⚠ 該機器人已存檔並停止開發以支持下一代機器人，請移至 https://gitlab.com/JC2048/redive-assistant-reloaded 

“出刀前請先報刀！”
<br>作為戰隊會長，要管理成員報刀實在太麻煩；報刀記錄又要人手整理...
<br>這個Discord機器人就是為了解決這個問題的答案。

這個機器人由[MiO9](https://mio9.sh/)及公連玩家JC合作進行編寫。
<br>小助手大部分內容會以繁體中文作為輸出，而大部分指令則接受繁簡中文及英語作為輸入。
<br>順便宣傳一下戰隊 [Origin](https://forum.gamer.com.tw/C.php?bsn=30861&snA=26461)。每個月要收人的話都有更新喔。

這個小助手暫時有以下功能：
- 記錄成員出刀，將報刀與出刀記錄掛鈎
- 完善的報刀功能，最低限度只需要1個指令就可以完成報刀+記錄出刀
- 報刀記錄同步至雲端資料庫，不會因意外遺失進度
- 自動生成及每日重置報刀列表
- 可自定多項報刀設定，例如可報刀周目等
- 補償刀計算支援


未來打算新增的功能有：
- 以 .csv 形式輸出出刀記錄，並生成傷害統計


![oxo](assets/bot-icon.jpg "oxo")

## 邀請連結

(邀請連結會在正式推出後提供)

## 指令

請到 Wiki:指令 閱讀詳細內容

<br><br>
<br>2020 ⚔Re:Dive 戰隊戰助手 by MiO9 & JC
<br>商標權等屬原提供者 (Cygames, So-net Taiwan)
<br>此機器人與超異域公主連結 Re:Dive 遊戲本身並無任何聯繫，亦不能修改或讀取遊戲數據
