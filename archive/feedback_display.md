## Syntax

¬: monospace spacing

`[]`: Values

`[week]`: fixed length as 2 space, add space if necessary

`{Entry}`: Knife report data

    [user-nickname] [knife-type] ([notes])
    (skip the ([notes]) part if empty)
=======: seperation line, seperates formatting and other stuff

## Knife Table

stage(week):

|week|stage|
|--:|:--:|
|1-3|1|
|4-10|2|
|11-34|3|
|>=35|4|

------
## Demo<br><br>

//本周：[week], 階段：[stage]
<br>//¬一王：[boss1name]
<br>//¬二王：[boss2name]
<br>//¬三王：[boss3name]
<br>//¬四王：[boss4name]
<br>//¬五王：[boss5name]

本周目--[current-week]周目
<br>¬一王： {Entry1}
<br>¬二王：
<br>**>三王： {Entry2}, {Entry3}<**
<br>¬四王： {Entry4}
<br>¬五王：

--------[current-week+1]周目
<br>¬一王： {Entry5}
<br>¬二王： {Entry6}
<br>¬三王： 
<br>¬四王： {Entry7}
<br>¬五王： {Entry8}, {Entry9}

--------[current-week+2]周目

(Continue until desired week)



table display:

    >16周3王 JC 新黑刀(一刀)

------

## Admin log (EMBED)

player-knife-counter: the order of knife of the player
<br>e.g. the player didnt have any knife report for today, then next report will be [ 1 ]


knife report

    <Big-Title>[user-nickname] 新增報刀
    <body>[knife-reporting-time] <第[player-knife-counter]刀> OR <補償刀>
    <title>報刀周目及種類：
    <body>[week]周[boss]王[knife-type], [notes]

    Title>      JC 新增報刀
                17:51:20 第2刀
    Subtitle>   報刀周目及種類：
                16周3王, 新黑刀, 一刀


    
cancel knife report

    <Big-Title>[user-nickname] 取消報刀
    <body>[knife-report-cancelling-time]
    <title>報刀周目：
    <body>[week]周[boss]王

    Title>      JC 取消報刀
                17:58:52 
    Subtitle>   報刀周目
                16周3王

------

## Alarm
Alarm Entry: <@user [knife-type]>



    討伐目標已更新至[stage]階段[week]周[boss]王。
    已報刀的玩家有：<{alarm-entry1}, {alarm-entry2}> OR (empty)

    討伐目標已更新至3階段16周3王。
    已報刀的玩家有：<@JC>
