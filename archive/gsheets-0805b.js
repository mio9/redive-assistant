// Google Sheets Functions

// Configurations

// Service account authentication
// Store the client secret in the same directory as this file
// And name the file "client_secret.json"

// Spreadsheet ID
// Make sure that the service account is granted edit rights to the spreadsheet
// https://docs.google.com/spreadsheets/d/[Your Sheet ID]/edit#gid=0
const DOC_ID = 'DOC_ID'

// END OF CONFIG

const { GoogleSpreadsheet } = require('google-spreadsheet');
const glog = "[Google Shits Services] ";
const glogi = glog + "[INFO] ";
const glogw = glog + "[WARN] ";

const doc = new GoogleSpreadsheet(DOC_ID);
let recordSheet; // Local record sheet cache object. Initialized and refreshed by refreshRecordSheet()

/**
 * Authenticates through Google and opens the spreadsheet specified.
 */
async function sheetInit() {
  await doc.useServiceAccountAuth(require('./client_secret.json'));
  await doc.loadInfo();
  console.log(`${glogi}Linked to Google Sheet: ${doc.title}`);

  await refreshRecordSheet(); // Initializes the knife record sheet and loads the content
};

/**
 * Returns current number of worksheets in the workbook.
 * Useful when providing index to rowWrite when you want to write to the last sheet.
 */
function sheetCount() {
  return doc.sheetCount;
}

/**
 * Returns the index of the sheet given the sheet title.
 * Returns -1 if the sheet is not found.
 * 
 * @param {String} title Title of the sheet to be found.
 */
function sheetIndexByName(title) {
  let num = sheetCount();
  for (let i = 0; i < num; i++) {
    let sheetTitle = doc.sheetsByIndex[i].title;
    if (sheetTitle === undefined) continue;
    if (sheetTitle.trim() === title.trim()) {
      return i;
    }
  }
  return -1;
}

/**
 * Copies an existing sheet.
 * Useful for copying a template sheet and creating a new sheet.
 * 
 * @param {Number} oldSheetIndex Index of the sheet to be copied. -1 if last.
 * @param {String} newSheetTitle Title for the new sheet.
 * @param {Number} newSheetIndex Index for the new sheet. -1 if last.
 */
async function sheetCopy(oldSheetIndex, newSheetTitle, newSheetIndex) {
  if (sheetIndexByName(newSheetTitle) !== -1) throw new Error('Sheet already exists!!');
  if (oldSheetIndex == -1) {
    await doc.loadInfo();
    oldSheetIndex = sheetCount() - 1;
  }
  return await doc.sheetsByIndex[oldSheetIndex].copyToSpreadsheet(DOC_ID).then( async () => {
    await doc.loadInfo();
    const lastSheet = sheetCount() - 1;
    if (newSheetIndex !== -1)
      await doc.sheetsByIndex[lastSheet].updateProperties({title: newSheetTitle, index: newSheetIndex});
    else
      await doc.sheetsByIndex[lastSheet].updateProperties({title: newSheetTitle});
  });
}

const writeLocation = ['C', 'J', 'Q'];
const writeSupplementLocation = ['G', 'N', 'U'];
/**
 * Records knife to the recordSheet.
 * 
 * @param {String} userid msg.author.username
 * @param {String} week 周目
 * @param {String} boss 王
 * @param {String} damage 傷害
 * @param {Boolean} last 尾刀. Ignored for 補刀
 * @param {Boolean} undo Internal flag to indicate undo actions. Should not be used. To undo, call undoKnife()
 * @param {Boolean} supplement Is supplement command
 */
async function recordKnife(userid, week, boss, damage, last, undo = false, supplement = false) {
  let knifeStatus = await retrieveKnife(userid);

  if (!supplement && knifeStatus.knifeSupplement === 1) {
    throw new Error ('Supplement not fulfilled!!');
  }
  
  // Modify values to undo knife
  if (undo) { 
    if (knifeStatus.knifeNo === 0) return; // Nothing more to undo?
    if (knifeStatus.knifeSupplement === 2) knifeStatus.knifeSupplement = 1;
    else {
      knifeStatus.knifeNo -= 1;
      knifeStatus.knifeSupplement = 0;
    }
    week = boss = damage = last = '';
  }

  // Data validation
  if (userid == undefined || isNaN(week) || isNaN(boss) || isNaN(damage) || last == undefined)
    throw new Error ('Invalid values!!');

  // New user?
  if (knifeStatus.found === false) {recordSheet.getCellByA1('B' + knifeStatus.row).value = userid};

  // Check if there are knives left
  if (knifeStatus.knifeNo === 3 && knifeStatus.knifeSupplement !== 1) throw new Error ('All knives used!!');

  // Resolve the supplement knife status
  try {
    if (knifeStatus.knifeSupplement === 1) { // Need supplement
      let loc = writeSupplementLocation[knifeStatus.knifeNo - 1];
      
      
      recordSheet.getCellByA1(loc + knifeStatus.row).value = week;
      recordSheet.getCellByA1(nextChar(loc, 1) + knifeStatus.row).value = boss;
      recordSheet.getCellByA1(nextChar(loc, 2) + knifeStatus.row).value = damage;
      
    }
    else { // No need supplement, move on to next knife
      let loc = writeLocation[knifeStatus.knifeNo];
      recordSheet.getCellByA1(loc + knifeStatus.row).value = week;
      recordSheet.getCellByA1(nextChar(loc, 1) + knifeStatus.row).value = boss;
      recordSheet.getCellByA1(nextChar(loc, 2) + knifeStatus.row).value = damage;
      recordSheet.getCellByA1(nextChar(loc, 3) + knifeStatus.row).value = (last ? 'V' : null);
    }
  }
  catch (error) {
    console.log(`${glogw}An error occurred while setting values`);
    console.log(`${glogw}The sheet will be reset to resolve the error state`);
    await refreshRecordSheet();
    console.log(`${glogw}Check the below error for details: \n${error}`);
    throw new Error (error);
  }

  recordSheet.saveUpdatedCells().then( () => {
    console.log(`${glogi}Successfully written knife record for ${userid}`);
  }).catch(async error => {
    console.error(`${glogw}An error occurred while writing, please check:`)
    console.log(error);
    console.log(`${glogw}The sheet will be reset to resolve the error state`);
    await refreshRecordSheet();
    throw new Error(error);
  });
}

function nextChar(char, offset) {
  return String.fromCharCode(char.charCodeAt(0) + offset);
}

/**
 * Undos the last knife recorded by user.
 * 
 * @param {String} userid msg.author.username
 */
async function undoKnife(userid) {
  return await recordKnife(userid, '', '', '', '', true);
}

const knifeCount = ['C', 'J', 'Q'];
const supplementKnife = ['F', 'M', 'T'];
const supplementKnifeFulfilled = ['G', 'N', 'U'];

/**
 * Retrieves the knife status of a user
 * Returns:
 *  row = row where the record of the user is found, first empty row if not found
 *  knifeNo = user out jor how many knifes, 0 if user not found
 *  knifeSupplement = 0 if 不需補刀, 1 if 未補刀, 2 if 補咗刀, 0 if user not found
 *  sheet = cells loaded in this function for the use of calling function
 *  found = whether user is found or not
 * 
 * @param {String} userid Discord user ID. msg.author.id
 */
async function retrieveKnife(userid) {
  let row = -1;
  let i = 3;
  for (i; i < recordSheet.rowCount; i++) {
    value = recordSheet.getCellByA1('B'+i).value;
    if (value === userid) {
      row = i;
      break;
    }
    if (value == null) break;
  }

  console.log(glogi + ((row === -1) ? `${userid} not found in sheet, new record will be written to row ${i}` : `Found ${userid} at row ${row}`));

  if (row === -1) return {row: i, knifeNo: 0, knifeSupplement: 0, sheet: recordSheet, found: false};

  // Determining current knife no.
  let knifeNo = -1;
  for (let i = 2; i >= 0; i--) {
    if (recordSheet.getCellByA1(knifeCount[i] + row).value != null) {
      knifeNo = i;
      break;
    }
  }
  knifeNo++;
  console.log(`${glogi}${userid} out jor knife ${knifeNo}`);

  if (knifeNo === 0) {
    console.log(`${glogi}Knife supplement status: User has undone all writes`);
    return {row: row, knifeNo: knifeNo, knifeSupplement: 0, found: true};
  }

  // Determining if supplement knife marked, knifeSupplement = 1 if required but not fulfilled
  let knifeSupplement = recordSheet.getCellByA1(supplementKnife[knifeNo - 1] + row).value != null;

  // Determining if supplement knife fulfilled, knifeSupplement = 2 if fulfilled
  knifeSupplement += knifeSupplement && recordSheet.getCellByA1(supplementKnifeFulfilled[knifeNo - 1] + row).value != null;
  console.log(`${glogi}Knife supplement status: ${knifeSupplement}`);

  return {row: row, knifeNo: knifeNo, knifeSupplement: knifeSupplement, found: true};
}

/**
 * Refreshes the local cached cells for the knife record sheet (first sheet). Should be called when the sheet is updated by other users.
 */
async function refreshRecordSheet() {
  await doc.loadInfo();
  console.log(`${glogi}Refreshed spreadsheet information`);
  const recordSheetIndex = 0;

  recordSheet = doc.sheetsByIndex[recordSheetIndex];
  await recordSheet.loadCells('B3:W');
  console.log(`${glogi}Loaded knife records of ${recordSheet.title}`)
}

/**
 * Copies the template sheet for next day.
 * The template sheet should be placed at the last sheet.
 */
async function nextDay() {
  await doc.loadInfo();
  let lastSheet = sheetCount() - 1;
  let date = new Date();
  let title = date.getDate().toString() + '/' + (date.getMonth() + 1);

  await sheetCopy(lastSheet, title, 0);
  console.log(`${glogi}Copied new sheet for ${title}`);
  await refreshRecordSheet();
  console.log(`${glogi}New sheet ready for writing`);
}

module.exports = {
  sheetInit,
  recordKnife,
  undoKnife,
  refreshRecordSheet,
  nextDay
}