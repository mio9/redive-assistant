/ either
# admin commands
* non-core features, can be implemented later
$ command result is in PM
<syntax> [optional syntax]

Unless otherwise stated: 
Normal command will be logged in a seperate channel available for everyone
Admin command will be logged in command log.

Storage Database name: knifeList


General commands:

.knife/.k <week> <boss> <notes>
    - Add a knife report to the knifeList

.list/.l <week> <boss>
    - List all knife reports of the boss

*.coop <week> <boss> <notes> <player> 
    - Add a cooperation knife report to the knifeList, including the command user and <player>
    - Generates 2 knifeList knife report entry of Coop = YES
*.addcoop <week> <boss> <notes> <player>
    - Modify an existing knife report as a cooperation knife with <player>. ONLY useable by the author of the knife report.
    - Modify 1 existing knife report entry to Coop = YES, and generates another one for <player>

.cancelknife/.ck -a/[week] [boss] 
    - No Argument: Delete last knife report done by user
    - [week] [boss]: Delete ALL knife report done by user of specific boss
    - -a: Delete ALL knife report done by user

* .undo
    - Delete last knife report done by user

>>>>>>  All of the above 4 command will generate a similar response:
        Your Knife have been reported/deleted. [Week] [Boss]: [list of queueing users]

.nextboss/.nb
    - Cycle the current boss to next one
    - 1 minute cooldown (can configure) for non-Admin

$*.myknife/.mk 
    - Generates a list of all of the knife reports done by the user since 5am until now. 

$* .list [week]
    - No Argument: Display knife report list of current week
    - [week]: Display knife report of a specific week 

* .end <week> <boss>
    - Mark an earliest knife report as "complete" in knifeList, on a specific boss if provided

# .set <week> <boss>
    - Set the current cycle to a specific week and boss

# .aknife/.ak/.ak47 <@user> <week> <boss>
    - Add a knife report for specific user to the knifeList

# .acancelknife/.ack <@user> <all> / .aknife <@user> <week> <boss>
    - all: Delete ALL knife report done by specific user
    - <week> <boss>: Delete ALL knife report done by specific user of specific boss

# .clear <week> [boss]
    - Clear ALL knife report of a specific week
    - [boss]: Clear ALL knife report of a specific boss
    * Will generate log of what have been deleted

#* .start
    - Enable the knife reporting procedure
    - Generate a new knifeList database file
#* .stop
    - Disable the knife reporting procedure
    - Clear ALL knife report in current knife table
    - Lock current knifeList database file
    - Need Captcha (9up)
    - Maybe some sort of password that I have to type again


Bot Main Functions:
1. Notify whenever a boss is available to a user who has reported knife on that boss
    e.g. the boss just go to week2 boss3, and @JC have reported week boss3, then the bot will sent a message like this to a SPECIFIC channel:
        Now is week2 boss3, reported knives are: JC (notes)


*2. Auto swap knife report order in specific conditions
    When a specific boss arrived, and the person who is responsible to it is not hitting it after a certain amount of time (e.g. 30 min)(configurable)

    Current list:
        Week 2
        1.
       >2. Player A (Knife 1)
        3.
        4. Player B (Knife 1)
        5.

        Week 3
        1. Player B (Knife 2)
        2. Player C (Knife 1)
        3.
        4.
        5. Player A (Knife 2)

        Week 4
        1.
        2.
        3.
        4.
        5.

    Details:
    For example:
    Player A has disappeared for 30 minute since the previous player uses .next to move on to boss 2-2
    1. First ask for the player responsible for Boss 3-2 (Player C Knife 1)
        If player C is ok (answer using another command, or just pressing a button), the system will do the swapping, and end this procedure.
        New list:
        Week 2
        1.
       >2. Player C (Knife 1)
        3.
        4. Player B (Knife 1)
        5.

        Week 3
        1. Player B (Knife 2)
       >2. Player A (Knife 1)
        3.
        4.
        5. Player A (Knife 2)
    
    2. After 5 minute (still ,configurable) if player C does not have any respond, the system will swap the corresponding knife to next available slot.
        New list:
        Week 2
        1.
       >2. Player A (Knife 1) (CANCELLED)
        3.
        4. Player B (Knife 1)
        5.

        Week 3
        1. Player B (Knife 2)
        2. Player C (Knife 1)
        3.
        4.
        5. Player A (Knife 2)

        Week 4
        1.
       >2. Player A (Knife 1)
        3.
        4.
        5.



3. Create a list at a specific text channel, which will display current week and future 2 week knife report.
Example:
    Current boss: 2-4
    JC: 2-3(Physics), 3-5+4-1 (Physics Combine), 4-1(Black Cat)<-- REPORTED AFTER NOJJ
    ChowKin1: 1-5 (Fun Fun Fun Knife), 2-4(Black Cat), 3-5(Physics)
    NoJJ: 4-1 (Compensation)
    (Following this order)
Display:
------------------------------------------------------------

    Current Boss: Week 2 Boss 4

    Week 2
     1:
     2:
     3: @JC (Physics)                    <-- will have crossing line, or grayed out above this week, since it passed already
    >4: @ChowKin1 (Black Cat)
     5:

    Week 3
     1: 
     2:
     3:
     4: 
     5: @JC (Physics Combine)

    Week 4
     1: @JC (Physics Combine, Black Cat), @NoJJ (Compensation)
     2:
     3:
     4:
     5:

The bot will keep editing the table, instead of sending a new message everytime; or delete the old table before sending new.
------------------------------------------------------------

Data set saved in database:

Now arranged in time and date

Player: Discord ID of the player
PlayerName: Player Nickname in the server
Time: Knife report generation time 
    (Uses UNIX, then display as yymmdd time those)
KnifeData:
    Week: Reported Week
    Boss: Reported Boss
    Type: Reported notes, probably will write another section to handle common knife types for ease of automation and statistics
    *Completed (boolean): Detect if the knife is outed or not.
    *CompleteTime: Get Time of Completion, also use UNIX
    *Coop (boolean): Detect whether the knife is a cooperation knife
    *CoopPlayer: Player Nickname in the server (empty or null if not a coop knife)
    *Damage: reported damage of the knife, incomplete knife always mark as 0
    *Score: Calculated score: Damage * BossMultiplier




Future Dev directions:
Add sub9 function
    Alert: Send big photo with alert message and @everyone, fun fun fun purpose
    Luck: Generate a luck base on discord ID and date, also fun fun fun purpose
    Compensation Calculator: Calculate the remaining time if 2 people combine knife in second. Actually useful.
        .compcal <Boss Blood/10k> <First Knife Damage/10k> <Second Knife Damage/10k>
        Formula: MAX((roundup((BossBlood-FirstKnife)/SecondKnife*90)+20),90)

More automation (After forcing everyone fill in their finished knife and damage):
    Auto score calculation: Will calculate their daily score
    Knife Counter: Count their knife enough or not, if run too many knife then will auto send warning.

--END--



20200826 2700




Changelog:
20200826 2700   First Draft

20200827 1725   Added:
                - Default command output 
                - Command result output modifier $
                - Proposed commands .coop/.addcoop/.myknife/.mk
                - Bot function: Auto swap knife order under conditions
                - Coop and CoopPlayer field in KnifeData
                - Changelog
                Edited:
                - Changed the [notes] field to a non-optional Argument
                - Changed .display/.dp to .list for obvious reasons

20200828 2234   - Added feedback message of knife reporting commands
                - Added a bit more detailed descriptions for coop knife commands         