// const { Command } = require("discord-akairo");
// const { UserManager, KnifeReportManager } = require("../api/storage-lib");
// const { MessageEmbed } = require("discord.js");
// const UtilLib = require("../api/util-lib");
// const strings = require("../lib/strings.json");
// const command = require("../lib/command-info.json");
// const parse = require("../api/parse-progress.js");
// const ReportContentHandler = require("../api/ReportContentHandler");
// const Log = require("../classes/LogContent");

// class UndoCommand extends Command {
//    constructor() {
//       super("undo", {
//          aliases: ["undo", "u"],
//          args: [
//             {
//                id: "week",
//                type: "integer"
//             },
//             {
//                id: "boss",
//                type: "integer",
//             },
//             {
//                id: "selection",
//                match: "flag",
//                flag: command.flags.selection
//             }
//          ],
//       });
//    };

//    async exec(message, args) {

//       let loadingMsg = await message.channel.send(UtilLib.randomLoading());
//       const avatarURL = message.author.displayAvatarURL()

//       // query completed reports
//       let query = {
//          user_id: message.author.id,
//          is_completed: true
//       };

//       if (args.week && args.boss) {
//          query.progress_num = parse.parse(args.week, args.boss);
//       };

//       const reportsArr = await KnifeReportManager.getReports(message.guild.id, query, "desc");

//       if (reportsArr.length == 0) { // no reports found

//          return loadingMsg.edit(strings.knifeReport.no_record);

//       } else {

//          let editReport;
//          if (args.selection == true) {

//             // list options
//             const listEmbed = new MessageEmbed()
//                .setColor("#e60000")
//                .setAuthor(`${reportsArr[0].data.username}`, avatarURL);

//             // loop data
//             for (let i in reportsArr) {

//                //pulling out data

//                let reportData = reportsArr[i].data;

//                const progress = parse.textUnparseAll(reportData.progress_num);

//                const type = reportData.type;
//                const comment = reportData.comment;

//                const date = new Date(reportData.time);
//                const time = UtilLib.formatDate(date);

//                let knifeTitle = reportData.is_compensate ? `🔸 ${progress} | ${type}` : `🔹 ${progress} | ${type}`;
//                if (reportData.is_completed == true) { // handle complete
//                   knifeTitle += " ✅";
//                };
//                const knifeDetail = comment ? `備註：${comment}\n時間：${time}` : `時間：${time}`

//                listEmbed.addField(knifeTitle, knifeDetail)
//             };
//             loadingMsg.edit(strings.undo.multiple_match_found);
//             loadingMsg.edit(listEmbed);

//             const selectionReturn = await UtilLib.emojiResponseSelection(message.author.id, loadingMsg, reportsArr.length);

//             if (selectionReturn == false) {  // handles selection timeout
//                return;
//             };

//             editReport = reportsArr[selectionReturn - 1];

//          } else {

//             // default: edit latest one
//             editReport = reportsArr[0];

//          };
//          const editedReport = await KnifeReportManager.edit(message.guild.id, editReport.id, { is_completed: false });
//          const returnEmbed = ReportContentHandler.composeEmbed(editedReport, "#60e0f0", null, avatarURL);

//          loadingMsg.edit(strings.undo.success); loadingMsg.edit(returnEmbed);
//          this.client.emit("knifeUpdate", message.guild);

//          const content = {
//             progressNum: editedReport.progress_num,
//             isCompensate: editedReport.is_compensate,
//             type: editedReport.type,
//             username: editedReport.username
//          }

//          const log = new Log("undo", content, "embed", avatarURL);
//          this.client.emit("broadcast", message.guild, log.output, "log");

//          return;
//       }

//    }
// }
// module.exports = UndoCommand